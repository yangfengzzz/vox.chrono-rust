/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use crate::ch_vector::ChVector;
use crate::ch_mathematics::{ch_clamp, ch_clamp_value};
use std::cmp::Ordering::{Less, Greater};
use crate::ch_frame::ChFrame;

///< maximum number of Newton iterations
const M_MAX_NUM_ITERS: usize = 50;
///< tolerance on squared distance
const M_SQR_DIST_TOL: f64 = 1e-6;
///< tolerance for orthogonality test
const M_COS_ANGLE_TOL: f64 = 1e-4;
///< tolerance for change in parameter value
const M_PARAM_TOL: f64 = 1e-8;

// -----------------------------------------------------------------------------
/// Definition of a piece-wise cubic Bezier approximation of a 3D curve.
///
/// This class encapsulates a piece-wise cubic Bezier approximation of a
/// 3D curve, represented as a set of three arrays of locations. For each
/// point on the curve, we also define a vector 'inCV' which represents the
/// vertex of the control polygon prior to the point and a vector 'outCV'
/// which represents the vertex of the control polygon following the point.
/// This class provides methods for evaluating the value, as well as the
/// first and second derivatives of a point on a specified interval of the
/// piece-wise 3D curve (using the Bernstein polynomial representation of
/// Bezier curves). In addition, it provides a method for calculating the
/// closest point on a specified interval of the curve to a specified
/// location.
// -----------------------------------------------------------------------------
pub struct ChBezierCurve {
    ///< set of knot points
    m_points: Vec<ChVector<f64>>,
    ///< set on "incident" control points
    m_in_cv: Vec<ChVector<f64>>,
    ///< set of "outgoing" control points
    m_out_cv: Vec<ChVector<f64>>,
}

impl ChBezierCurve {
    /// Constructor from specified nodes and control points.
    pub fn new(points: &Vec<ChVector<f64>>, in_cv: &Vec<ChVector<f64>>, out_cv: &Vec<ChVector<f64>>) -> ChBezierCurve {
        debug_assert!(points.len() > 1);
        debug_assert!(points.len() == in_cv.len());
        debug_assert!(points.len() == out_cv.len());
        return ChBezierCurve {
            m_points: points.clone(),
            m_in_cv: in_cv.clone(),
            m_out_cv: out_cv.clone(),
        };
    }

    /// Constructor from specified nodes.
    /// In this case, we evaluate the control polygon vertices inCV and outCV
    /// so that we obtain a piecewise cubic spline interpolate of the given knots.
    pub fn new_nodes(points: &Vec<ChVector<f64>>) -> ChBezierCurve {
        let mut curve = ChBezierCurve {
            m_points: vec![],
            m_in_cv: vec![],
            m_out_cv: vec![],
        };

        let num_points = points.len();
        debug_assert!(num_points > 1);

        curve.m_in_cv.resize(num_points, ChVector::new_default());
        curve.m_out_cv.resize(num_points, ChVector::new_default());

        curve.m_in_cv[0] = points[0].clone();
        curve.m_out_cv[num_points - 1] = points[num_points - 1].clone();

        // Special case for two points only.  In this case, the curve should be a
        // straight line.
        if num_points == 2 {
            curve.m_out_cv[0] = (points[0].clone() * 2.0 + points[1].clone()) / 3.0;
            curve.m_in_cv[1] = (points[0].clone() + points[1].clone() * 2.0) / 3.0;
            return curve;
        }

        // Calculate coordinates of the outCV control points.
        let n = num_points - 1;
        let mut rhs: Vec<f64> = Vec::new();
        rhs.resize(n, 0.0);
        let mut x: Vec<f64> = Vec::new();
        x.resize(n, 0.0);
        let mut y: Vec<f64> = Vec::new();
        y.resize(n, 0.0);
        let mut z: Vec<f64> = Vec::new();
        z.resize(n, 0.0);

        // X coordinates.
        for i in 1..(n - 1) {
            rhs[i] = 4.0 * *points[i].x() + 2.0 * *points[i + 1].x();
        }
        rhs[0] = points[0].x() + 2.0 * points[1].x();
        rhs[n - 1] = (8.0 * points[n - 1].x() + points[n].x()) / 2.0;
        ChBezierCurve::solve_tri_diag(&mut rhs, &mut x);

        // Y coordinates.
        for i in 1..(n - 1) {
            rhs[i] = 4.0 * points[i].y() + 2.0 * points[i + 1].y();
        }
        rhs[0] = points[0].y() + 2.0 * points[1].y();
        rhs[n - 1] = (8.0 * points[n - 1].y() + points[n].y()) / 2.0;
        ChBezierCurve::solve_tri_diag(&mut rhs, &mut y);

        // Z coordinates.
        for i in 1..(n - 1) {
            rhs[i] = 4.0 * points[i].z() + 2.0 * points[i + 1].z();
        }
        rhs[0] = points[0].z() + 2.0 * points[1].z();
        rhs[n - 1] = (8.0 * points[n - 1].z() + points[n].z()) / 2.0;
        ChBezierCurve::solve_tri_diag(&mut rhs, &mut z);

        // Set control points outCV and inCV.
        for i in 0..(n - 1) {
            curve.m_out_cv[i] = ChVector::new(x[i], y[i], z[i]);
            curve.m_in_cv[i + 1] = ChVector::new(points[i + 1].x() * 2.0 - x[i + 1],
                                                 points[i + 1].y() * 2.0 - y[i + 1],
                                                 points[i + 1].z() * 2.0 - z[i + 1]);
        }
        curve.m_out_cv[n - 1] = ChVector::new(x[n - 1], y[n - 1], z[n - 1]);
        curve.m_in_cv[n] = ChVector::new((points[n].x() + x[n - 1]) / 2.0,
                                         (points[n].y() + y[n - 1]) / 2.0,
                                         (points[n].z() + z[n - 1]) / 2.0);

        return curve;
    }

    /// Default constructor (required by serialization)
    pub fn new_default() -> ChBezierCurve {
        return ChBezierCurve {
            m_points: vec![],
            m_in_cv: vec![],
            m_out_cv: vec![],
        };
    }
}

impl ChBezierCurve {
    /// Set the nodes and control points
    pub fn set_points(&mut self, points: &Vec<ChVector<f64>>, in_cv: &Vec<ChVector<f64>>, out_cv: &Vec<ChVector<f64>>) {
        debug_assert!(points.len() > 1);
        debug_assert!(points.len() == in_cv.len());
        debug_assert!(points.len() == out_cv.len());
        self.m_points = points.clone();
        self.m_in_cv = in_cv.clone();
        self.m_out_cv = out_cv.clone();
    }

    /// Return the number of knot points.
    pub fn get_num_points(&self) -> usize { return self.m_points.len(); }

    /// Return the knot point with specified index.
    pub fn get_point(&self, i: usize) -> &ChVector<f64> { return &self.m_points[i]; }

    /// evaluate the value of the Bezier curve.
    /// This function calculates and returns the point on the curve at the
    /// given curve parameter (assumed to be in [0,1]).
    /// A value t=0 returns the first point on the curve.
    /// A value t=1 returns the last point on the curve.
    pub fn eval(&self, t: f64) -> ChVector<f64> {
        let par = ch_clamp(t, 0.0, 1.0);
        let num_intervals = self.get_num_points() - 1;
        let epar = par * num_intervals as f64;
        let mut i = f64::floor(par * num_intervals as f64) as usize;
        ch_clamp_value(&mut i, 0, num_intervals - 1);

        return self.eval_interval(i, epar - i as f64);
    }

    /// evaluate the value of the Bezier curve.
    /// This function calculates and returns the point on the curve in the
    /// specified interval between two knot points and at the given curve
    /// parameter (assumed to be in [0,1]).
    /// A value t-0 returns the first end of the specified interval.
    /// A value t=1 return the second end of the specified interval.
    /// It uses the Bernstein polynomial representation of a Bezier curve.
    pub fn eval_interval(&self, i: usize, t: f64) -> ChVector<f64> {
        debug_assert!(i >= 0 && i < self.get_num_points() - 1);

        let omt = 1.0 - t;
        let t2 = t * t;
        let omt2 = omt * omt;

        let b0 = omt * omt2;
        let b1 = 3.0 * t * omt2;
        let b2 = 3.0 * t2 * omt;
        let b3 = t * t2;

        return &self.m_points[i] * b0 + &self.m_out_cv[i] * b1 + &self.m_in_cv[i + 1] * b2 + &self.m_points[i + 1] * b3;
    }

    /// evaluate the tangent vector to the Bezier curve.
    /// This function calculates and returns the first derivative (tangent vector)
    /// to the curve in the specified interval between two knot points and at the
    /// given curve parameter (assumed to be in [0,1]). It uses the Bernstein
    /// polynomial representation of a Bezier curve.
    pub fn eval_d(&self, i: usize, t: f64) -> ChVector<f64> {
        debug_assert!(i >= 0 && i < self.get_num_points() - 1);

        let omt = 1.0 - t;
        let t2 = t * t;
        let omt2 = omt * omt;

        let b0 = -3.0 * omt2;
        let b1 = 3.0 * omt2 - 6.0 * t * omt;
        let b2 = 6.0 * t * omt - 3.0 * t2;
        let b3 = 3.0 * t2;

        return &self.m_points[i] * b0 + &self.m_out_cv[i] * b1 + &self.m_in_cv[i + 1] * b2 + &self.m_points[i + 1] * b3;
    }

    /// evaluate the second derivative vector to the Bezier curve.
    /// This function calculates and returns the second derivative vector to the
    /// curve in the specified interval between two knot points and at the given
    /// curve parameter (assumed to be in [0,1]). It uses the Bernstein polynomial
    /// representation of a Bezier curve.
    pub fn eval_dd(&self, i: usize, t: f64) -> ChVector<f64> {
        debug_assert!(i >= 0 && i < self.get_num_points() - 1);

        let omt = 1.0 - t;

        let b0 = 6.0 * omt;
        let b1 = -12.0 * omt + 6.0 * t;
        let b2 = 6.0 * omt - 12.0 * t;
        let b3 = 6.0 * t;

        return &self.m_points[i] * b0 + &self.m_out_cv[i] * b1 + &self.m_in_cv[i + 1] * b2 + &self.m_points[i + 1] * b3;
    }

    /// Calculate the closest point on the curve to the given location.
    /// This function calculates and returns the point on the curve in the specified
    /// interval that is closest to the specified location. On input, the value 't' is
    /// an initial guess. On return, it contains the curve parameter corresponding
    /// to the closest point.
    pub fn calc_closest_point(&self, loc: &ChVector<f64>, i: usize, t: &mut f64) -> ChVector<f64> {
        let mut q = self.eval_interval(i, *t);
        let mut qd = ChVector::new_default();
        let mut qdd = ChVector::new_default();

        for j in 0..M_MAX_NUM_ITERS {
            let vec = &q - loc;
            let d2 = vec.length2();

            if d2 < M_SQR_DIST_TOL {
                break;
            }

            qd = self.eval_d(i, *t);

            let dot = vec.dot(&qd);
            let cos_angle = dot / (d2.sqrt() * qd.length());

            if cos_angle.abs() < M_COS_ANGLE_TOL {
                break;
            }

            qdd = self.eval_dd(i, *t);

            let dt = dot / (vec.dot(&qdd) + qd.length2());

            *t -= dt;

            if *t < M_PARAM_TOL || *t > 1.0 - M_PARAM_TOL {
                ch_clamp_value(t, 0.0, 1.0);
                q = self.eval_interval(i, *t);
                break;
            }

            q = self.eval_interval(i, *t);

            if (qd * dt).length2() < M_SQR_DIST_TOL {
                break;
            }
        };

        return q;
    }

    /// Utility function to solve for the outCV control points.
    /// This function solves the resulting tridiagonal system for one of the
    /// coordinates (x, y, or z) of the outCV control points, to impose that the
    /// resulting Bezier curve is a spline interpolate of the knots.
    pub fn solve_tri_diag(rhs: &mut Vec<f64>, x: &mut Vec<f64>) {
        let n = rhs.len();
        let mut tmp: Vec<f64> = Vec::new();
        tmp.resize(n, 0.0);

        let mut b = 2.0;
        x[0] = rhs[0] / b;

        // Decomposition and forward substitution.
        for i in 1..n {
            tmp[i] = 1.0 / b;
            b = match i < n - 1 {
                true => 4.0,
                false => 3.5
            } - tmp[i];
            x[i] = (rhs[i] - x[i - 1]) / b;
        }

        // Back substitution.
        for i in 1..n {
            x[n - i - 1] -= tmp[n - i] * x[n - i];
        }
    }
}

// -----------------------------------------------------------------------------
/// Definition of a tracker on a ChBezierCurve path.
///
/// This utility class implements a tracker for a given path. It uses time
/// coherence in order to provide an appropriate initial guess for the
/// iterative (Newton) root finder.
// -----------------------------------------------------------------------------
struct ChBezierCurveTracker<'a> {
    ///< associated Bezier curve
    m_path: &'a ChBezierCurve,
    ///< current search interval
    m_cur_interval: usize,
    ///< parameter for current closest point
    m_cur_param: f64,
    ///< treat the path as a closed loop curve
    m_is_closed_path: bool,
}

impl<'a> ChBezierCurveTracker<'a> {
    /// Create a tracker associated with the specified Bezier curve.
    pub fn new(path: &'a ChBezierCurve, is_closed_path: Option<bool>) -> ChBezierCurveTracker<'a> {
        return ChBezierCurveTracker {
            m_path: path,
            m_cur_interval: 0,
            m_cur_param: 0.0,
            m_is_closed_path: is_closed_path.unwrap_or(false),
        };
    }

    /// Reset the tracker at the specified location.
    /// This function reinitialize the pathTracker at the specified location. It
    /// calculates an appropriate initial guess for the curve segment and sets the
    /// curve parameter to 0.5.
    pub fn reset(&mut self, loc: &ChVector<f64>) {
        // Walk all curve points and calculate the distance to the specified reset
        // location, then sort them in increasing order.
        let mut points: Vec<PointSpec> = Vec::new();

        for i in 0..self.m_path.get_num_points() {
            points.push(PointSpec::new(i, (loc - &self.m_path.m_points[i]).length2()));
        }
        points.sort_by(|p1, p2| {
            return match p1.m_dist2 < p2.m_dist2 {
                true => Less,
                false => Greater
            };
        });

        // Set the initial guess to be at t=0.5 in either the interval starting at
        // the point with minimum distance or in the previous interval.
        self.m_cur_param = 0.5;
        self.m_cur_interval = points[0].m_index;

        if self.m_cur_interval == 0 {
            return;
        }

        if self.m_cur_interval == self.m_path.get_num_points() - 1 {
            self.m_cur_interval -= 1;
            return;
        }

        let loc2cur = &self.m_path.m_points[self.m_cur_interval] - loc;
        let loc2prev = &self.m_path.m_points[self.m_cur_interval - 1] - loc;

        if loc2cur.dot(&loc2prev) < 0.0 {
            self.m_cur_interval -= 1;
        }
    }

    /// Calculate the closest point on the underlying curve to the specified location.
    /// This function returns the closest point on the underlying path to the
    /// specified location. The return value is -1 if this point coincides with the
    /// first point of the path, +1 if it coincides with the last point of the path,
    /// and 0 otherwise. Note that, in order to provide a reasonable initial guess
    /// for the Newton iteration, we use time coherence (by keeping track of the path
    /// interval and curve parameter within that interval from the last query). As
    /// such, this function should be called with a continuous sequence of locations.
    pub fn calc_closest_point(&mut self, loc: &ChVector<f64>, point: &mut ChVector<f64>) -> i32 {
        let mut last_at_min = false;
        let mut last_at_max = false;

        loop {
            *point = self.m_path.calc_closest_point(loc, self.m_cur_interval, &mut self.m_cur_param);

            if self.m_cur_param < M_PARAM_TOL {
                if (self.m_cur_interval == 0) && (!self.m_is_closed_path) {
                    return -1;
                }
                if last_at_max {
                    return 0;
                }

                // If the search region is at the beginning of the interval check the
                // previous interval.  Loop to the last interval if the path is a
                // closed loop and is it is currently in the first interval
                if (self.m_cur_interval == 0) && (self.m_is_closed_path) {
                    self.m_cur_interval = self.m_path.get_num_points() - 2;
                } else {
                    self.m_cur_interval -= 1;
                }
                last_at_min = true;
                self.m_cur_param = 1.0;
            } else if self.m_cur_param > 1.0 - M_PARAM_TOL {
                if (self.m_cur_interval == self.m_path.get_num_points() - 2) && (!self.m_is_closed_path) {
                    return 1;
                }

                if last_at_min {
                    return 0;
                }

                // If the search region is at the end of the interval check the
                // next interval.  Loop to the first interval if the path is a
                // closed loop and is it is currently in the last interval
                if (self.m_cur_interval == self.m_path.get_num_points() - 2) && (self.m_is_closed_path) {
                    self.m_cur_interval = 0;
                } else {
                    self.m_cur_interval += 1;
                }
                last_at_max = true;
                self.m_cur_param = 0.0;
            } else {
                return 0;
            }
        }
    }

    /// Calculate the closest point on the underlying curve to the specified location.
    /// Return the TNB (tangent-normal-binormal) frame and the curvature at the closest point.
    /// The ChFrame 'tnb' has X axis along the tangent, Y axis along the normal, and Z axis
    /// along the binormal.  The frame location is the closest point on the Bezier curve.
    /// Note that the normal and binormal are not defined at points with zero curvature.
    /// In such cases, we return an orthonormal frame with X axis along the tangent.
    pub fn calc_closest_point_with_frame(loc: &ChVector<f64>, tnb: &ChFrame<f64>, curvature: &f64) -> i32 {
        todo!()
    }

    /// Set if the path is treated as an open loop or a closed loop for tracking
    pub fn set_is_closed_path(&mut self, is_closed_path: bool) {
        self.m_is_closed_path = is_closed_path;
    }
}

struct PointSpec {
    m_index: usize,
    m_dist2: f64,
}

impl PointSpec {
    fn new_default() -> PointSpec {
        return PointSpec {
            m_index: 0,
            m_dist2: 0.0,
        };
    }
    fn new(index: usize, dist2: f64) -> PointSpec {
        return PointSpec {
            m_index: index,
            m_dist2: dist2,
        };
    }
}
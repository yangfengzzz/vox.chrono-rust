/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

pub enum BCType {
    ///< quadratic function over corresponding interval (first/last)
    DefaultBc,
    ///< imposed first derivative at corresponding endpoint (left/right)
    FirstBc,
    ///< imposed second derivative at corresponding endpoint (left/right)
    SecondBc,
}

/// Implementation of 1-D piece-wise cubic spline curves.
pub struct ChCubicSpline {
    m_process: bool,

    m_left_bc_type: BCType,
    m_right_bc_type: BCType,

    m_left_bc: f64,
    m_right_bc: f64,

    m_t: Vec<f64>,
    m_y: Vec<f64>,
    m_ypp: Vec<f64>,
}

impl ChCubicSpline {
    /// Construct a cubic spline y = y(t).
    /// It is assumed that the t values are sorted in ascending order.
    pub fn new(t: &Vec<f64>, y: &Vec<f64>) -> ChCubicSpline {
        let mut spline = ChCubicSpline {
            m_process: true,
            m_left_bc_type: BCType::DefaultBc,
            m_right_bc_type: BCType::DefaultBc,
            m_left_bc: 0.0,
            m_right_bc: 0.0,
            m_t: t.clone(),
            m_y: y.clone(),
            m_ypp: vec![],
        };

        let n = t.len();
        debug_assert!(n >= 2);
        debug_assert!(n == y.len());

        // debug_assert!(t.is_sorted());
        spline.m_ypp.resize(n, 0.0);

        return spline;
    }

    /// Set the boundary condition at the left endpoint.
    /// The specified value is ignored if type = DEFAULT_BC.
    pub fn set_left_bc(&mut self, bc_type: BCType, val: f64) {
        self.m_left_bc_type = bc_type;
        self.m_left_bc = val;
        self.m_process = true;
    }

    /// Set the boundary condition at the right endpoint.
    /// The specified value is ignored if type = DEFAULT_BC.
    pub fn set_right_bc(&mut self, bc_type: BCType, val: f64) {
        self.m_right_bc_type = bc_type;
        self.m_right_bc = val;
        self.m_process = true;
    }
}

impl ChCubicSpline {
    /// evaluate the cubic spline at the specified value t.
    pub fn evaluate(&mut self, t: f64, y: &mut f64, yp: &mut f64, ypp: &mut f64) {
        if self.m_process {
            self.process();
        }

        let n = self.m_t.len();
        debug_assert!(t >= self.m_t[0]);
        debug_assert!(t <= self.m_t[n - 1]);

        // Bracket the given value: m_t[i_left] < t <= m_t[i_left+1]
        let mut left = 0;
        for i in 1..n {
            if t > self.m_t[i] {
                left += 1;
            } else {
                break;
            }
        }

        let dt = self.m_t[left + 1] - self.m_t[left];
        let tau = (t - self.m_t[left]) / dt;
        let a = dt * dt * (self.m_ypp[left + 1] - self.m_ypp[left]) / 6.0;
        let b = dt * dt * self.m_ypp[left] / 2.0;
        let c = (self.m_y[left + 1] - self.m_y[left]) - a - b;
        let d = self.m_y[left];

        *y = a * tau * tau * tau + b * tau * tau + c * tau + d;
        *yp = (3.0 * a * tau * tau + 2.0 * b * tau + c) / dt;
        *ypp = (6.0 * a * tau + 2.0 * b) / (dt * dt);
    }

    fn process(&mut self) {
        todo!()
    }
}
/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use crate::ch_coordsys::ChCoordsys;

/// ChFrame: a class for coordinate systems in 3D space.
///
///  A 'frame' coordinate system has a translation and
/// a rotation respect to a 'parent' coordinate system,
/// usually the absolute (world) coordinates.
///
///  Differently from a simple ChCoordsys object, however,
/// the ChFrame implements some optimizations because
/// each ChFrame stores also a 3x3 rotation matrix, which
/// can speed up coordinate transformations when a large
/// amount of vectors must be transformed by the same
/// coordinate frame.
///
/// Further info at the @ref coordinate_transformations manual page.

pub struct ChFrame<Real: Float> {
    ///< Rotation and position, as vector+quaternion
    pub coord: ChCoordsys<Real>,
}
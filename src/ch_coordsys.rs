/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use crate::ch_vector::{ChVector, VNULL};
use crate::ch_quaternion::{ChQuaternion, QNULL, QUNIT};
use num::traits::FloatConst;
use std::ops::*;
use std::fmt::Display;

///
/// COORDSYS:
///
///  This class contains both translational variable
/// (the origin of the axis) and the rotational variable
/// (that is the unitary quaternion which represent the
/// special-orthogonal transformation matrix).
///   Basic features for point-coordinate transformations
/// are provided. However, for more advanced features, the
/// heavier classes ChFrame() or ChFrameMoving() may suit better.
///  The coordsys object comes either with the template "ChCoordsys<type>" mode,
/// either in the 'shortcut' flavor, that is "Coordsys", which assumes
/// the type of the four scalars is double precision, so it is faster to type.
///
/// Further info at the @ref coordinate_transformations manual page.
#[derive(Clone)]
pub struct ChCoordsys<Real: Float> {
    pub pos: ChVector<Real>,
    pub rot: ChQuaternion<Real>,
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> ChCoordsys<Real> {
    // Default constructor (identity frame)
    pub fn new_default() -> ChCoordsys<Real> {
        return ChCoordsys {
            pos: ChVector::new_default(),
            rot: ChQuaternion::new(Real::one(), Real::zero(), Real::zero(), Real::zero()),
        };
    }

    // Construct from position and rotation (as quaternion)
    pub fn new(mv: &ChVector<Real>, mq: Option<ChQuaternion<Real>>) -> ChCoordsys<Real> {
        return ChCoordsys {
            pos: mv.clone(),
            rot: mq.unwrap_or(ChQuaternion::new(Real::one(), Real::zero(), Real::zero(), Real::zero())),
        };
    }

    // Construct from position mv and rotation of angle alpha around unit vector mu
    pub fn new_ang_aix(mv: &ChVector<Real>, alpha: Real, mu: &ChVector<Real>) -> ChCoordsys<Real> {
        let mut coord = ChCoordsys::new(mv, None);
        coord.rot.q_from_ang_axis(alpha, mu);
        return coord;
    }

    /// Force to z=0, and z rotation only. No normalization to quaternion, however.
    pub fn force2d(&mut self) {
        *self.pos.z_mut() = Real::zero();
        *self.rot.e1_mut() = Real::zero();
        *self.rot.e2_mut() = Real::zero();
    }

    /// Force 3d coordsys to lie on a XY plane (note: no normalize. on quat)
    pub fn force2d_csys(&self) -> ChCoordsys<Real> {
        let mut res = self.clone();
        *res.pos.z_mut() = Real::zero();
        *res.rot.e1_mut() = Real::zero();
        *res.rot.e2_mut() = Real::zero();
        return res;
    }

    /// Returns true if coordsys is identical to other coordsys
    pub fn equals(&self, other: &ChCoordsys<Real>) -> bool {
        return self.rot.equals(&other.rot) && self.pos.equals(&other.pos);
    }

    /// Returns true if coordsys is equal to other coordsys, within a tolerance 'tol'
    pub fn equals_tol(&self, other: &ChCoordsys<Real>, tol: Real) -> bool {
        return self.rot.equals_tol(&other.rot, tol) && self.pos.equals_tol(&other.pos, tol);
    }

    /// Sets to no translation and no rotation
    pub fn set_identity(&mut self) {
        self.pos = ChVector::new_default();
        self.rot = ChQuaternion::new(Real::one(), Real::zero(), Real::zero(), Real::zero());
    }
}

/// # FUNCTIONS TO TRANSFORM THE FRAME ITSELF
impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> ChCoordsys<Real> {
    /// Apply a transformation (rotation and translation) represented by
    /// another ChCoordsys t. This is equivalent to pre-multiply this csys
    /// by the other csys t:   this'= t * this; or this' = this >> t
    pub fn concatenate_pre_transformation(&mut self, t: &ChCoordsys<Real>) {
        self.pos = t.transform_vec_local_to_parent(&self.pos);
        self.rot = &t.rot * &self.rot;
    }

    /// Apply a transformation (rotation and translation) represented by
    /// another ChCoordsys t in local coordinate. This is equivalent to
    /// post-multiply this csys by the other csys t:   this'= this * t; or this'= t >> this
    pub fn concatenate_post_transformation(&mut self, t: &ChCoordsys<Real>) {
        self.pos = self.transform_vec_local_to_parent(&t.pos);
        self.rot = &self.rot * &t.rot;
    }

    // FUNCTIONS FOR COORDINATE TRANSFORMATIONS

    /// This function transforms a point from the local coordinate
    /// system to the parent coordinate system. Relative position of local respect
    /// to parent is given by this coordys, i.e. 'origin' translation and 'alignment' quaternion.
    /// \return The point in parent coordinate, as parent=origin +q*[0,(local)]*q'
    pub fn transform_vec_local_to_parent(&self, local: &ChVector<Real>) -> ChVector<Real> {
        return &self.pos + self.rot.rotate(local);
    }

    pub fn transform_point_local_to_parent(&self, local: &ChVector<Real>) -> ChVector<Real> {
        return &self.pos + self.rot.rotate(local);
    }

    /// This function transforms a point from the parent coordinate
    /// system to a local coordinate system, whose relative position
    /// is given by this coodsys, i.e. 'origin' translation and 'alignment' quaternion.
    /// \return The point in local coordinate, as local=q'*[0,(parent-origin)]*q
    pub fn transform_vec_parent_to_local(&self, parent: &ChVector<Real>) -> ChVector<Real> {
        return self.rot.rotate_back(&(parent - &self.pos));
    }

    pub fn transform_point_parent_to_local(&self, parent: &ChVector<Real>) -> ChVector<Real> {
        return self.rot.rotate_back(&(parent - &self.pos));
    }

    /// This function transforms a direction from 'this' local coordinate system
    /// to the parent coordinate system.
    pub fn transform_direction_local_to_parent(&self, local: &ChVector<Real>) -> ChVector<Real> {
        return self.rot.rotate(local);
    }

    /// This function transforms a direction from the parent coordinate system to
    /// 'this' local coordinate system.
    pub fn transform_direction_parent_to_local(&self, parent: &ChVector<Real>) -> ChVector<Real> {
        return self.rot.rotate_back(parent);
    }

    /// This function transforms a coordsys given in 'this' coordinate system to
    /// the parent coordinate system
    pub fn transform_local_to_parent(&self, local: &ChCoordsys<Real>) -> ChCoordsys<Real> {
        return ChCoordsys::new(&self.transform_vec_local_to_parent(&local.pos), Some(&self.rot % &local.rot));
    }

    /// This function transforms a coordsys given in the parent coordinate system
    /// to 'this' coordinate system
    pub fn transform_parent_to_local(&self, parent: &ChCoordsys<Real>) -> ChCoordsys<Real> {
        return ChCoordsys::new(&self.transform_vec_parent_to_local(&parent.pos), Some(self.rot.get_conjugate() % &parent.rot));
    }
}

//--------------------------------------------------------------------------------------------------
impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> Shr<ChCoordsys<Real>> for ChCoordsys<Real> {
    type Output = ChCoordsys<Real>;

    /// The '>>' operator transforms a coordinate system, so
    /// transformations can be represented with this syntax:
    ///  new_frame = old_frame >> tr_frame;
    /// For a sequence of transformations, i.e. a chain of coordinate
    /// systems, you can also write this (like you would do with
    /// a sequence of Denavitt-Hartemberg matrix multiplications,
    /// but in the _opposite_ order...)
    ///  new_frame = old_frame >> frame3to2 >> frame2to1 >> frame1to0;
    /// This operation is not commutative.
    fn shr(self, rhs: ChCoordsys<Real>) -> Self::Output {
        return rhs.transform_local_to_parent(&self);
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> Shr<ChVector<Real>> for ChCoordsys<Real> {
    type Output = ChCoordsys<Real>;

    /// The '>>' operator that transforms 'mixed' types:
    ///  frame_C = frame_A >> vector_B;
    /// where frame_A  is  a ChCoordsys
    ///       vector_B is  a ChVector
    /// Returns a ChCoordsys.
    /// The effect is like applying the translation vector_B to frame_A and get frame_C.
    fn shr(self, rhs: ChVector<Real>) -> Self::Output {
        let mut res = self.clone();
        res.pos += rhs;
        return res;
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> Shr<ChCoordsys<Real>> for ChVector<Real> {
    type Output = ChVector<Real>;

    /// The '>>' operator that transforms 'mixed' types:
    ///  vector_C = vector_A >> frame_B;
    /// where vector_A is  a ChVector
    ///       frame_B  is  a ChCoordsys
    /// Returns a ChVector.
    /// The effect is like applying the transformation frame_B to frame_A and get frame_C.
    fn shr(self, rhs: ChCoordsys<Real>) -> Self::Output {
        return rhs.transform_point_local_to_parent(&self);
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> Shr<ChQuaternion<Real>> for ChCoordsys<Real> {
    type Output = ChCoordsys<Real>;

    /// The '>>' operator that transforms 'mixed' types:
    ///  frame_C = frame_A >> quat_B;
    /// where frame_A is  a ChCoordsys
    ///       frame_B is  a ChQuaternion
    /// Returns a ChCoordsys.
    /// The effect is like applying the rotation quat_B to frame_A and get frame_C.
    fn shr(self, rhs: ChQuaternion<Real>) -> Self::Output {
        return ChCoordsys::new(&rhs.rotate(&self.pos), Some(self.rot >> rhs));
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> Shr<ChCoordsys<Real>> for ChQuaternion<Real> {
    type Output = ChQuaternion<Real>;

    /// The '>>' operator that transforms 'mixed' types:
    ///  quat_C = quat_A >> frame_B;
    /// where quat_A   is  a ChQuaternion
    ///       frame_B  is  a ChCoordsys
    /// Returns a ChQuaternion.
    /// The effect is like applying the transformation frame_B to quat_A and get quat_C.
    fn shr(self, rhs: ChCoordsys<Real>) -> Self::Output {
        return self >> rhs.rot;
    }
}

//--------------------------------------------------------------------------------------------------
impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> Mul<ChCoordsys<Real>> for ChCoordsys<Real> {
    type Output = ChCoordsys<Real>;

    /// The '*' operator transforms a coordinate system, so
    /// transformations can be represented with this syntax:
    ///  new_frame = tr_frame * old_frame;
    /// For a sequence of transformations, i.e. a chain of coordinate
    /// systems, you can also write this (just like you would do with
    /// a sequence of Denavitt-Hartemberg matrix multiplications!)
    ///  new_frame = frame1to0 * frame2to1 * frame3to2 * old_frame;
    /// This operation is not commutative.
    ///  NOTE: since c++ operator execution is from left to right, in
    /// case of multiple transformations like w=A*B*C*v, the >> operator
    /// performs faster, like  w=v>>C>>B>>A;
    fn mul(self, rhs: ChCoordsys<Real>) -> Self::Output {
        return self.transform_local_to_parent(&rhs);
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> Mul<ChVector<Real>> for ChCoordsys<Real> {
    type Output = ChVector<Real>;

    /// The '*' operator that transforms 'mixed' types:
    ///  vector_C = frame_A * vector_B;
    /// where frame_A  is  a ChCoordsys
    ///       vector_B is  a ChVector
    /// Returns a ChVector.
    /// The effect is like applying the transformation frame_A to vector_B and get vector_C.
    fn mul(self, rhs: ChVector<Real>) -> Self::Output {
        return self.transform_point_local_to_parent(&rhs);
    }
}

impl<Real: Float + AddAssign> Mul<ChCoordsys<Real>> for ChVector<Real> {
    type Output = ChCoordsys<Real>;

    /// The '*' operator that transforms 'mixed' types:
    ///  frame_C = vector_A * frame_B;
    /// where vector_A is  a ChVector
    ///       frame_B  is  a ChCoordsys
    /// Returns a ChCoordsys.
    /// The effect is like applying the translation vector_A to frame_B and get frame_C.
    fn mul(self, rhs: ChCoordsys<Real>) -> Self::Output {
        let mut res = rhs.clone();
        res.pos += self;
        return res;
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> Mul<ChQuaternion<Real>> for ChCoordsys<Real> {
    type Output = ChQuaternion<Real>;

    /// The '*' operator that transforms 'mixed' types:
    ///  quat_C = frame_A * quat_B;
    /// where frame_A  is  a ChCoordsys
    ///       quat_B   is  a ChQuaternion
    /// Returns a ChQuaternion.
    /// The effect is like applying the transformation frame_A to quat_B and get quat_C.
    fn mul(self, rhs: ChQuaternion<Real>) -> Self::Output {
        return self.rot * rhs;
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> Mul<ChCoordsys<Real>> for ChQuaternion<Real> {
    type Output = ChCoordsys<Real>;

    /// The '*' operator that transforms 'mixed' types:
    ///  frame_C = quat_A * frame_B;
    /// where quat_A   is  a ChQuaternion
    ///       frame_B  is  a ChCoordsys
    /// Returns a ChCoordsys.
    /// The effect is like applying the rotation quat_A to frame_B and get frame_C.
    fn mul(self, rhs: ChCoordsys<Real>) -> Self::Output {
        return ChCoordsys::new(&self.rotate(&rhs.pos), Some(self * rhs.rot));
    }
}

//--------------------------------------------------------------------------------------------------
impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> Div<ChCoordsys<Real>> for ChCoordsys<Real> {
    type Output = ChCoordsys<Real>;

    /// The '/' is like the '*' operator (see), but uses the inverse
    /// transformation for A, in A/b. (with A ChFrame, b ChVector)
    /// That is: c=A*b ; b=A/c;
    fn div(self, rhs: ChCoordsys<Real>) -> Self::Output {
        return self.transform_parent_to_local(&rhs);
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> ShrAssign<ChCoordsys<Real>> for ChCoordsys<Real> {
    /// Performs pre-multiplication of this frame by another
    /// frame, for example: A>>=T means  A'=T*A ; or A'=A >> T
    fn shr_assign(&mut self, rhs: ChCoordsys<Real>) {
        self.concatenate_pre_transformation(&rhs);
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> MulAssign<ChCoordsys<Real>> for ChCoordsys<Real> {
    /// Performs post-multiplication of this frame by another
    /// frame, for example: A*=T means  A'=A*T ; or A'=T >> A
    fn mul_assign(&mut self, rhs: ChCoordsys<Real>) {
        self.concatenate_post_transformation(&rhs);
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> ShrAssign<ChVector<Real>> for ChCoordsys<Real> {
    /// Performs pre-multiplication of this frame by a vector D, to 'move' by a displacement D:
    fn shr_assign(&mut self, rhs: ChVector<Real>) {
        self.pos += rhs;
    }
}

impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> ShrAssign<ChQuaternion<Real>> for ChCoordsys<Real> {
    /// Performs pre-multiplication of this frame by a quaternion R, to 'rotate' it by R:
    fn shr_assign(&mut self, rhs: ChQuaternion<Real>) {
        self.pos = rhs.rotate(&self.pos);
        self.rot = rhs * &self.rot;
    }
}

/// Shortcut for faster use of typical double-precision coordsys.
///  Instead of writing    "ChCoordsys<double> foo;"   you can write
///  the shorter version   "Coordsys foo;"
///
pub type Coordsys = ChCoordsys<f64>;

/// Shortcut for faster use of typical single-precision coordsys.
///
pub type CoordsysF = ChCoordsys<f32>;

// CONSTANTS

pub const CSYSNULL: ChCoordsys<f64> = ChCoordsys {
    pos: VNULL,
    rot: QNULL,
};
pub const CSYSNORM: ChCoordsys<f64> = ChCoordsys {
    pos: VNULL,
    rot: QUNIT,
};
/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use num::traits::FloatConst;
use std::ops::*;
use std::cmp::Ordering;
use std::fmt::Display;
use crate::ch_mathematics::*;
use crate::ch_vector::ChVector;

/// Definitions of various angle sets for conversions.
#[derive(Clone)]
pub enum AngleSet {
    AngleAxis,
    ///< sequence: Z - X' - Z''
    EULERO,
    ///< sequence: Z - X' - Y''
    CARDANO,
    ///< sequence:
    HPB,
    ///< sequence: X - Y' - Z''
    RXYZ,
    RODRIGUEZ,
    QUATERNION,
}

/// Class defining quaternion objects, that is four-dimensional numbers, also known as Euler parameters.
/// Quaternions are very useful when used to represent rotations in 3d.
///
/// Further info at the @ref manual_ChQuaternion  manual page.
#[derive(Clone)]
pub struct ChQuaternion<Real: Float> {
    /// Data in the order e0, e1, e2, e3
    pub(crate) m_data: [Real; 4],
}

impl<Real: Float> ChQuaternion<Real> {
    /// Default constructor.
    /// Note that this constructs a null quaternion {0,0,0,0}, not a {1,0,0,0} unit quaternion.
    pub fn new_default() -> ChQuaternion<Real> {
        return ChQuaternion {
            m_data: [Real::zero(); 4]
        };
    }

    /// Constructor from four scalars. The first is the real part, others are i,j,k imaginary parts
    pub fn new(e0: Real, e1: Real, e2: Real, e3: Real) -> ChQuaternion<Real> {
        return ChQuaternion {
            m_data: [e0, e1, e2, e3]
        };
    }

    /// Constructor from real part, and vector with i,j,k imaginary part.
    pub fn new_vec(s: Real, v: &ChVector<Real>) -> ChQuaternion<Real> {
        return ChQuaternion {
            m_data: [s, *v.x(), *v.y(), *v.z()]
        };
    }

    /// Access to components
    pub fn e0(&self) -> &Real { return &self.m_data[0]; }
    pub fn e1(&self) -> &Real { return &self.m_data[1]; }
    pub fn e2(&self) -> &Real { return &self.m_data[2]; }
    pub fn e3(&self) -> &Real { return &self.m_data[3]; }
    pub fn e0_mut(&mut self) -> &mut Real { return &mut self.m_data[0]; }
    pub fn e1_mut(&mut self) -> &mut Real { return &mut self.m_data[1]; }
    pub fn e2_mut(&mut self) -> &mut Real { return &mut self.m_data[2]; }
    pub fn e3_mut(&mut self) -> &mut Real { return &mut self.m_data[3]; }

    /// Return const pointer to underlying array storage.
    pub fn data(&self) -> &[Real] { return &self.m_data; }

    // SET & GET FUNCTIONS

    /// Sets the four values of the quaternion at once
    pub fn set(&mut self, e0: Real, e1: Real, e2: Real, e3: Real) {
        self.m_data[0] = e0;
        self.m_data[1] = e1;
        self.m_data[2] = e2;
        self.m_data[3] = e3;
    }

    /// Sets the quaternion as a copy of another quaternion
    pub fn set_quat(&mut self, q: &ChQuaternion<Real>) {
        self.m_data[0] = q.m_data[0];
        self.m_data[1] = q.m_data[1];
        self.m_data[2] = q.m_data[2];
        self.m_data[3] = q.m_data[3];
    }

    /// Sets the quaternion with four components as a sample scalar
    pub fn set_identity(&mut self, s: Real) {
        self.m_data[0] = s;
        self.m_data[1] = s;
        self.m_data[2] = s;
        self.m_data[3] = s;
    }

    /// Sets the quaternion as a null quaternion
    pub fn set_null(&mut self) {
        self.m_data[0] = Real::zero();
        self.m_data[1] = Real::zero();
        self.m_data[2] = Real::zero();
        self.m_data[3] = Real::zero();
    }

    /// Sets the quaternion as a unit quaternion
    pub fn set_unit(&mut self) {
        self.m_data[0] = Real::one();
        self.m_data[1] = Real::zero();
        self.m_data[2] = Real::zero();
        self.m_data[3] = Real::zero();
    }

    /// Sets the scalar part only
    pub fn set_scalar(&mut self, s: Real) {
        self.m_data[0] = s;
    }

    /// Sets the vectorial part only
    pub fn set_vector(&mut self, v: &ChVector<Real>) {
        self.m_data[1] = *v.x();
        self.m_data[2] = *v.y();
        self.m_data[3] = *v.z();
    }

    /// Return true if quaternion is identical to other quaternion
    pub fn equals(&self, other: &ChQuaternion<Real>) -> bool {
        return (other.m_data[0] == self.m_data[0])
            && (other.m_data[1] == self.m_data[1])
            && (other.m_data[2] == self.m_data[2])
            && (other.m_data[3] == self.m_data[3]);
    }

    /// Return true if quaternion equals another quaternion, within a tolerance 'tol'
    pub fn equals_tol(&self, other: &ChQuaternion<Real>, tol: Real) -> bool {
        return ((other.m_data[0] - self.m_data[0]).abs() < tol) && ((other.m_data[1] - self.m_data[1]).abs() < tol) &&
            ((other.m_data[2] - self.m_data[2]).abs() < tol) && ((other.m_data[3] - self.m_data[3]).abs() < tol);
    }

    /// Gets the vectorial part only
    pub fn get_vector(&self) -> ChVector<Real> {
        return ChVector::new(self.m_data[1], self.m_data[2], self.m_data[3]);
    }

    /// Get the X axis of a coordinate system, given the quaternion which represents
    /// the alignment of the coordinate system. Note that it is assumed that the
    /// quaternion is already normalized.
    pub fn get_x_axis(&self) -> ChVector<Real> {
        let two = Real::from(2.0).unwrap();
        return ChVector::new((self.m_data[0] * self.m_data[0] + self.m_data[1] * self.m_data[1]) * two - Real::one(),
                             (self.m_data[1] * self.m_data[2] + self.m_data[0] * self.m_data[3]) * two,
                             (self.m_data[1] * self.m_data[3] - self.m_data[0] * self.m_data[2]) * two);
    }

    /// Get the Y axis of a coordinate system, given the quaternion which represents
    /// the alignment of the coordinate system. Note that it is assumed that the
    /// quaternion is already normalized.
    pub fn get_y_axis(&self) -> ChVector<Real> {
        let two = Real::from(2.0).unwrap();
        return ChVector::new((self.m_data[1] * self.m_data[2] - self.m_data[0] * self.m_data[3]) * two,
                             (self.m_data[0] * self.m_data[0] + self.m_data[2] * self.m_data[2]) * two - Real::one(),
                             (self.m_data[2] * self.m_data[3] + self.m_data[0] * self.m_data[1]) * two);
    }

    /// Get the Z axis of a coordinate system, given the quaternion which represents
    /// the alignment of the coordinate system. Note that it is assumed that the
    /// quaternion is already normalized.
    pub fn get_z_axis(&self) -> ChVector<Real> {
        let two = Real::from(2.0).unwrap();
        return ChVector::new((self.m_data[1] * self.m_data[3] + self.m_data[0] * self.m_data[2]) * two,
                             (self.m_data[2] * self.m_data[3] - self.m_data[0] * self.m_data[1]) * two,
                             (self.m_data[0] * self.m_data[0] + self.m_data[3] * self.m_data[3]) * two - Real::one());
    }

    // QUATERNION NORMS

    /// Compute the euclidean norm of the quaternion, that is its length or magnitude.
    pub fn length(&self) -> Real {
        return self.length2().sqrt();
    }

    /// Compute the squared euclidean norm of the quaternion.
    pub fn length2(&self) -> Real {
        return self.dot(self);
    }

    /// Compute the infinity norm of the quaternion, that is the maximum absolute value of one of its elements.
    pub fn length_inf(&self) -> Real {
        let e0e1 = Real::max(Real::abs(self.m_data[0]), Real::abs(self.m_data[1]));
        let e0e1e2 = Real::max(e0e1, Real::abs(self.m_data[2]));
        return Real::max(e0e1e2, Real::abs(self.m_data[3]));
    }
}

//--------------------------------------------------------------------------------------------------
impl<Real: Float> Index<usize> for ChQuaternion<Real> {
    type Output = Real;

    fn index(&self, index: usize) -> &Self::Output {
        return &self.m_data[index];
    }
}

impl<Real: Float> IndexMut<usize> for ChQuaternion<Real> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self.m_data[index];
    }
}

impl<Real: Float> Neg for ChQuaternion<Real> {
    type Output = ChQuaternion<Real>;

    fn neg(self) -> Self::Output {
        return ChQuaternion::new(-self.m_data[0], -self.m_data[1], -self.m_data[2], -self.m_data[3]);
    }
}

impl<Real: Float> Neg for &ChQuaternion<Real> {
    type Output = ChQuaternion<Real>;

    fn neg(self) -> Self::Output {
        return ChQuaternion::new(-self.m_data[0], -self.m_data[1], -self.m_data[2], -self.m_data[3]);
    }
}

impl<Real: Float> Not for &ChQuaternion<Real> {
    type Output = ChQuaternion<Real>;

    fn not(self) -> Self::Output {
        return ChQuaternion::new(self.m_data[0], -self.m_data[1], -self.m_data[2], -self.m_data[3]);
    }
}

macro_rules! impl_add {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float> Add<$OTHER> for $SELF {
            type Output = ChQuaternion<Real>;

            fn add(self, rhs: $OTHER) -> Self::Output {
                return ChQuaternion::new(self.m_data[0] + rhs.m_data[0],
                                         self.m_data[1] + rhs.m_data[1],
                                         self.m_data[2] + rhs.m_data[2],
                                         self.m_data[3] + rhs.m_data[3]);
            }
        }
    };
}
impl_add!(ChQuaternion<Real>, ChQuaternion<Real>);
impl_add!(ChQuaternion<Real>, &ChQuaternion<Real>);
impl_add!(&ChQuaternion<Real>, ChQuaternion<Real>);
impl_add!(&ChQuaternion<Real>, &ChQuaternion<Real>);

macro_rules! impl_add_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + AddAssign> AddAssign<$OTHER> for $SELF {
            fn add_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] += rhs.m_data[0];
                self.m_data[1] += rhs.m_data[1];
                self.m_data[2] += rhs.m_data[2];
                self.m_data[3] += rhs.m_data[3];
            }
        }
    };
}
impl_add_assign!(ChQuaternion<Real>, ChQuaternion<Real>);
impl_add_assign!(ChQuaternion<Real>, &ChQuaternion<Real>);

macro_rules! impl_sub {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float> Sub<$OTHER> for $SELF {
            type Output = ChQuaternion<Real>;

            fn sub(self, rhs: $OTHER) -> Self::Output {
                return ChQuaternion::new(self.m_data[0] - rhs.m_data[0],
                                        self.m_data[1] - rhs.m_data[1],
                                        self.m_data[2] - rhs.m_data[2],
                                        self.m_data[3] - rhs.m_data[3]);
            }
        }
    };
}
impl_sub!(ChQuaternion<Real>, ChQuaternion<Real>);
impl_sub!(ChQuaternion<Real>, &ChQuaternion<Real>);
impl_sub!(&ChQuaternion<Real>, ChQuaternion<Real>);
impl_sub!(&ChQuaternion<Real>, &ChQuaternion<Real>);

macro_rules! impl_sub_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + SubAssign> SubAssign<$OTHER> for $SELF {
            fn sub_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] -= rhs.m_data[0];
                self.m_data[1] -= rhs.m_data[1];
                self.m_data[2] -= rhs.m_data[2];
                self.m_data[3] -= rhs.m_data[3];
            }
        }
    };
}
impl_sub_assign!(ChQuaternion<Real>, ChQuaternion<Real>);
impl_sub_assign!(ChQuaternion<Real>, &ChQuaternion<Real>);

macro_rules! impl_mul {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real:Float> Mul<$OTHER> for $SELF {
            type Output = ChQuaternion<Real>;

            fn mul(self, rhs: $OTHER) -> Self::Output {
                let mut q = ChQuaternion::new_default();
                q.cross_assign(&self, rhs);
                return q;
            }
        }
    };
}
impl_mul!(ChQuaternion<Real>, &ChQuaternion<Real>);
impl_mul!(&ChQuaternion<Real>, &ChQuaternion<Real>);

macro_rules! impl_mul_ref {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real:Float> Mul<$OTHER> for $SELF {
            type Output = ChQuaternion<Real>;

            fn mul(self, rhs: $OTHER) -> Self::Output {
                let mut q = ChQuaternion::new_default();
                q.cross_assign(&self, &rhs);
                return q;
            }
        }
    };
}
impl_mul_ref!(ChQuaternion<Real>, ChQuaternion<Real>);
impl_mul_ref!(&ChQuaternion<Real>, ChQuaternion<Real>);

impl<Real: Float + MulAssign> MulAssign<ChQuaternion<Real>> for ChQuaternion<Real> {
    fn mul_assign(&mut self, rhs: ChQuaternion<Real>) {
        self.cross_assign(&self.clone(), &rhs);
    }
}

impl<Real: Float + MulAssign> MulAssign<&ChQuaternion<Real>> for ChQuaternion<Real> {
    fn mul_assign(&mut self, rhs: &ChQuaternion<Real>) {
        self.cross_assign(&self.clone(), rhs);
    }
}

macro_rules! impl_shr {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real:Float> Shr<$OTHER> for $SELF {
            type Output = ChQuaternion<Real>;

            fn shr(self, rhs: $OTHER) -> Self::Output {
                let mut q = ChQuaternion::new_default();
                q.cross_assign(&rhs, &self);
                return q;
            }
        }
    };
}
impl_shr!(ChQuaternion<Real>, &ChQuaternion<Real>);
impl_shr!(&ChQuaternion<Real>, &ChQuaternion<Real>);
impl_shr!(ChQuaternion<Real>, ChQuaternion<Real>);
impl_shr!(&ChQuaternion<Real>, ChQuaternion<Real>);

macro_rules! impl_shr_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real:Float> ShrAssign<$OTHER> for $SELF {
            fn shr_assign(&mut self, rhs: $OTHER) {
                self.cross_assign(&rhs, &self.clone());
            }
        }
    };
}
impl_shr_assign!(ChQuaternion<Real>, ChQuaternion<Real>);
impl_shr_assign!(ChQuaternion<Real>, &ChQuaternion<Real>);

macro_rules! impl_mul_scalar {
    ($SELF:ty) => {
        impl<Real:Float> Mul<Real> for $SELF {
            type Output = ChQuaternion<Real>;

            fn mul(self, rhs: Real) -> Self::Output {
                return ChQuaternion::new(self.m_data[0] * rhs,
                                        self.m_data[1] * rhs,
                                        self.m_data[2] * rhs,
                                        self.m_data[3] * rhs);
            }
        }
    };
}
impl_mul_scalar!(ChQuaternion<Real>);
impl_mul_scalar!(&ChQuaternion<Real>);

impl<Real: Float + MulAssign> MulAssign<Real> for ChQuaternion<Real> {
    fn mul_assign(&mut self, rhs: Real) {
        self.m_data[0] *= rhs;
        self.m_data[1] *= rhs;
        self.m_data[2] *= rhs;
        self.m_data[3] *= rhs;
    }
}

macro_rules! impl_div {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real:Float> Div<$OTHER> for $SELF {
            type Output = ChQuaternion<Real>;

            fn div(self, rhs: $OTHER) -> Self::Output {
                return ChQuaternion::new(self.m_data[0] / rhs.m_data[0],
                                        self.m_data[1] / rhs.m_data[1],
                                        self.m_data[2] / rhs.m_data[2],
                                        self.m_data[3] / rhs.m_data[3]);
            }
        }
    };
}
impl_div!(ChQuaternion<Real>, ChQuaternion<Real>);
impl_div!(ChQuaternion<Real>, &ChQuaternion<Real>);
impl_div!(&ChQuaternion<Real>, ChQuaternion<Real>);
impl_div!(&ChQuaternion<Real>, &ChQuaternion<Real>);

macro_rules! impl_div_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + DivAssign> DivAssign<$OTHER> for $SELF {
            fn div_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] /= rhs.m_data[0];
                self.m_data[1] /= rhs.m_data[1];
                self.m_data[2] /= rhs.m_data[2];
                self.m_data[3] /= rhs.m_data[3];
            }
        }
    };
}
impl_div_assign!(ChQuaternion<Real>, ChQuaternion<Real>);
impl_div_assign!(ChQuaternion<Real>, &ChQuaternion<Real>);

macro_rules! impl_div_scalar {
    ($SELF:ty) => {
        impl<Real:Float> Div<Real> for $SELF {
            type Output = ChQuaternion<Real>;

            fn div(self, rhs: Real) -> Self::Output {
                return ChQuaternion::new(self.m_data[0] / rhs,
                                        self.m_data[1] / rhs,
                                        self.m_data[2] / rhs,
                                        self.m_data[3] / rhs);
            }
        }
    };
}
impl_div_scalar!(ChQuaternion<Real>);
impl_div_scalar!(&ChQuaternion<Real>);

impl<Real: Float + DivAssign> DivAssign<Real> for ChQuaternion<Real> {
    fn div_assign(&mut self, rhs: Real) {
        self.m_data[0] /= rhs;
        self.m_data[1] /= rhs;
        self.m_data[2] /= rhs;
        self.m_data[3] /= rhs;
    }
}

impl<Real: Float> BitXor for ChQuaternion<Real> {
    type Output = Real;

    fn bitxor(self, rhs: Self) -> Self::Output {
        return self.dot(&rhs);
    }
}

impl<Real: Float> Rem<ChQuaternion<Real>> for ChQuaternion<Real> {
    type Output = ChQuaternion<Real>;

    fn rem(self, rhs: ChQuaternion<Real>) -> Self::Output {
        let mut v = ChQuaternion::new_default();
        v.cross_assign(&self, &rhs);
        return v;
    }
}

impl<Real: Float> Rem<&ChQuaternion<Real>> for ChQuaternion<Real> {
    type Output = ChQuaternion<Real>;

    fn rem(self, rhs: &ChQuaternion<Real>) -> Self::Output {
        let mut v = ChQuaternion::new_default();
        v.cross_assign(&self, rhs);
        return v;
    }
}

impl<Real: Float> Rem<&ChQuaternion<Real>> for &ChQuaternion<Real> {
    type Output = ChQuaternion<Real>;

    fn rem(self, rhs: &ChQuaternion<Real>) -> Self::Output {
        let mut v = ChQuaternion::new_default();
        v.cross_assign(&self, rhs);
        return v;
    }
}

impl<Real: Float> RemAssign for ChQuaternion<Real> {
    fn rem_assign(&mut self, rhs: Self) {
        self.cross_assign(&self.clone(), &rhs);
    }
}

//--------------------------------------------------------------------------------------------------
/// # FUNCTIONS
impl<Real: Float> ChQuaternion<Real> {
    /// Set this quaternion to the quaternion product of the two quaternions A and B,
    /// following the classic Hamilton rule:  this = AxB.
    /// This is the true, typical quaternion product. It is NOT commutative.
    pub fn cross_assign(&mut self, qa: &ChQuaternion<Real>, qb: &ChQuaternion<Real>) {
        let w = qa.m_data[0] * qb.m_data[0] - qa.m_data[1] * qb.m_data[1] - qa.m_data[2] * qb.m_data[2] -
            qa.m_data[3] * qb.m_data[3];
        let x = qa.m_data[0] * qb.m_data[1] + qa.m_data[1] * qb.m_data[0] - qa.m_data[3] * qb.m_data[2] +
            qa.m_data[2] * qb.m_data[3];
        let y = qa.m_data[0] * qb.m_data[2] + qa.m_data[2] * qb.m_data[0] + qa.m_data[3] * qb.m_data[1] -
            qa.m_data[1] * qb.m_data[3];
        let z = qa.m_data[0] * qb.m_data[3] + qa.m_data[3] * qb.m_data[0] - qa.m_data[2] * qb.m_data[1] +
            qa.m_data[1] * qb.m_data[2];
        self.m_data[0] = w;
        self.m_data[1] = x;
        self.m_data[2] = y;
        self.m_data[3] = z;
    }

    /// Return the dot product with another quaternion: result = this ^ b.
    pub fn dot(&self, b: &ChQuaternion<Real>) -> Real {
        return (self.m_data[0] * b.m_data[0]) + (self.m_data[1] * b.m_data[1])
            + (self.m_data[2] * b.m_data[2]) + (self.m_data[3] * b.m_data[3]);
    }
}

impl<Real: Float + MulAssign> ChQuaternion<Real> {
    /// scale this quaternion by a scalar: this *= s.
    pub fn scale(&mut self, s: Real) {
        self.m_data[0] *= s;
        self.m_data[1] *= s;
        self.m_data[2] *= s;
        self.m_data[3] *= s;
    }

    /// normalize this quaternion in place, so that its euclidean length is 1.
    /// Return false if the original quaternion had zero length (in which case the quaternion
    /// is set to [1,0,0,0]) and return true otherwise.
    pub fn normalize(&mut self) -> bool {
        let length = self.length();
        if length < Real::min_positive_value() {
            self.m_data[0] = Real::one();
            self.m_data[1] = Real::zero();
            self.m_data[2] = Real::zero();
            self.m_data[3] = Real::zero();
            return false;
        }
        self.scale(Real::one() / length);
        return true;
    }

    /// Return a normalized copy of this quaternion, with euclidean length = 1.
    /// Not to be confused with normalize() which normalizes in place.
    pub fn get_normalized(&self) -> ChQuaternion<Real> {
        let mut q = self.clone();
        q.normalize();
        return q;
    }

    /// Set this quaternion to the conjugate of the a quaternion.
    pub fn set_conjugate(&mut self, a: &ChQuaternion<Real>) {
        self.m_data[0] = a.m_data[0];
        self.m_data[1] = -a.m_data[1];
        self.m_data[2] = -a.m_data[2];
        self.m_data[3] = -a.m_data[3];
    }

    /// conjugate this quaternion in place (its vectorial part changes sign).
    pub fn conjugate(&mut self) {
        self.set_conjugate(&self.clone());
    }

    /// Return a conjugated version of this quaternion.
    pub fn get_conjugate(&self) -> ChQuaternion<Real> {
        return ChQuaternion::new(self.m_data[0], -self.m_data[1], -self.m_data[2], -self.m_data[3]);
    }

    /// Return the inverse of this quaternion.
    pub fn get_inverse(&self) -> ChQuaternion<Real> {
        let mut invq = self.get_conjugate();
        invq.scale(Real::one() / self.length2());
        return invq;
    }
}

/// # TRANSFORMATIONS
impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> ChQuaternion<Real> {
    /// rotate the vector a on the basis of this quaternion: res=p*[0,a]*p'
    /// (speed-optimized version). Endomorphism assumes p is already normalized.
    pub fn rotate(&self, a: &ChVector<Real>) -> ChVector<Real> {
        let two = Real::from(2.0).unwrap();
        let e0e0 = self.m_data[0] * self.m_data[0];
        let e1e1 = self.m_data[1] * self.m_data[1];
        let e2e2 = self.m_data[2] * self.m_data[2];
        let e3e3 = self.m_data[3] * self.m_data[3];
        let e0e1 = self.m_data[0] * self.m_data[1];
        let e0e2 = self.m_data[0] * self.m_data[2];
        let e0e3 = self.m_data[0] * self.m_data[3];
        let e1e2 = self.m_data[1] * self.m_data[2];
        let e1e3 = self.m_data[1] * self.m_data[3];
        let e2e3 = self.m_data[2] * self.m_data[3];
        return ChVector::new(((e0e0 + e1e1) * two - Real::one()) * *a.x() + ((e1e2 - e0e3) * two) * *a.y() + ((e1e3 + e0e2) * two) * *a.z(),
                             ((e1e2 + e0e3) * two) * *a.x() + ((e0e0 + e2e2) * two - Real::one()) * *a.y() + ((e2e3 - e0e1) * two) * *a.z(),
                             ((e1e3 - e0e2) * two) * *a.x() + ((e2e3 + e0e1) * two) * *a.y() + ((e0e0 + e3e3) * two - Real::one()) * *a.z());
    }

    /// rotate the vector a on the basis of conjugate of this quaternion: res=p'*[0,a]*p
    /// (speed-optimized version).  Endomorphism assumes p is already normalized.
    pub fn rotate_back(&self, a: &ChVector<Real>) -> ChVector<Real> {
        let two = Real::from(2.0).unwrap();
        let e0e0 = self.m_data[0] * self.m_data[0];
        let e1e1 = self.m_data[1] * self.m_data[1];
        let e2e2 = self.m_data[2] * self.m_data[2];
        let e3e3 = self.m_data[3] * self.m_data[3];
        let e0e1 = -self.m_data[0] * self.m_data[1];
        let e0e2 = -self.m_data[0] * self.m_data[2];
        let e0e3 = -self.m_data[0] * self.m_data[3];
        let e1e2 = self.m_data[1] * self.m_data[2];
        let e1e3 = self.m_data[1] * self.m_data[3];
        let e2e3 = self.m_data[2] * self.m_data[3];
        return ChVector::new(((e0e0 + e1e1) * two - Real::one()) * *a.x() + ((e1e2 - e0e3) * two) * *a.y() + ((e1e3 + e0e2) * two) * *a.z(),
                             ((e1e2 + e0e3) * two) * *a.x() + ((e0e0 + e2e2) * two - Real::one()) * *a.y() + ((e2e3 - e0e1) * two) * *a.z(),
                             ((e1e3 - e0e2) * two) * *a.x() + ((e2e3 + e0e1) * two) * *a.y() + ((e0e0 + e3e3) * two - Real::one()) * *a.z());
    }

    // CONVERSIONS

    /// Set the quaternion from a rotation vector (ie. a 3D axis of rotation with length as angle of rotation)
    /// defined in absolute coords.
    /// If you need distinct axis and angle, use q_from_ang_axis().
    pub fn q_from_rot_v(&mut self, angle_axis: &ChVector<Real>) {
        let theta_squared = angle_axis.length2();
        // For non-zero rotation:
        if theta_squared > Real::zero() {
            let theta = theta_squared.sqrt();
            let half_theta = theta / Real::from(2.0).unwrap();
            let k = half_theta.sin() / theta;
            self.m_data[0] = half_theta.cos();
            self.m_data[1] = *angle_axis.x() * k;
            self.m_data[2] = *angle_axis.y() * k;
            self.m_data[3] = *angle_axis.z() * k;
        } else {
            // For almost zero rotation:
            let k = Real::from(0.5).unwrap();
            self.m_data[0] = Real::one();
            self.m_data[1] = *angle_axis.x() * k;
            self.m_data[2] = *angle_axis.y() * k;
            self.m_data[3] = *angle_axis.z() * k;
        }
    }

    /// Get the rotation vector (ie. a 3D axis of rotation with length as angle of rotation) from a quaternion.
    /// If you need distinct axis and angle, use rather q_to_ang_axis().
    pub fn q_to_rot_v(&self) -> ChVector<Real> {
        let mut angle_axis = ChVector::new_default();
        let sin_squared = self.m_data[1] * self.m_data[1] + self.m_data[2] * self.m_data[2] + self.m_data[3] * self.m_data[3];
        // For non-zero rotation
        if sin_squared > Real::zero() {
            let sin_theta = sin_squared.sqrt();
            let k = Real::from(2.0).unwrap() * Real::atan2(sin_theta, self.m_data[0]) / sin_theta;
            *angle_axis.x_mut() = self.m_data[1] * k;
            *angle_axis.y_mut() = self.m_data[2] * k;
            *angle_axis.z_mut() = self.m_data[3] * k;
        } else {
            // For almost zero rotation
            let k = Real::from(2.0).unwrap();
            *angle_axis.x_mut() = self.m_data[1] * k;
            *angle_axis.y_mut() = self.m_data[2] * k;
            *angle_axis.z_mut() = self.m_data[3] * k;
        }
        return angle_axis;
    }

    /// Set the quaternion from an angle of rotation and an axis, defined in absolute coords.
    /// The axis is supposed to be fixed, i.e. it is constant during rotation!
    /// NOTE, axis must be normalized!
    /// If you need directly the rotation vector=axis * angle, use q_from_rot_v().
    pub fn q_from_ang_axis(&mut self, angle: Real, axis: &ChVector<Real>) {
        let halfang = angle / Real::from(2.0).unwrap();
        let sinhalf = halfang.sin();
        self.m_data[0] = halfang.cos();
        self.m_data[1] = *axis.x() * sinhalf;
        self.m_data[2] = *axis.y() * sinhalf;
        self.m_data[3] = *axis.z() * sinhalf;
    }

    /// Set the quaternion from an angle of rotation about X axis.
    pub fn q_from_ang_x(&mut self, angle_x: Real) {
        self.q_from_ang_axis(angle_x, &ChVector::new(Real::one(), Real::zero(), Real::zero()));
    }

    /// Set the quaternion from an angle of rotation about Y axis.
    pub fn q_from_ang_y(&mut self, angle_y: Real) {
        self.q_from_ang_axis(angle_y, &ChVector::new(Real::zero(), Real::one(), Real::zero()));
    }

    /// Set the quaternion from an angle of rotation about Z axis.
    pub fn q_from_ang_z(&mut self, angle_z: Real) {
        self.q_from_ang_axis(angle_z, &ChVector::new(Real::zero(), Real::zero(), Real::one()));
    }

    /// Convert the quaternion to an angle of rotation and an axis, defined in absolute coords.
    /// Resulting angle and axis must be passed as parameters.
    /// Note that angle is in [-PI....+PI] range. Also remember  (angle, axis) is the same of (-angle,-axis).
    /// If you need directly the rotation vector=axis * angle, use q_to_rotv().
    pub fn q_to_ang_axis(&self, a_angle: &mut Real, a_axis: &mut ChVector<Real>) {
        let two = Real::from(2.0).unwrap();
        let sin_squared = self.m_data[1] * self.m_data[1] + self.m_data[2] * self.m_data[2] + self.m_data[3] * self.m_data[3];
        // For non-zero rotation
        if sin_squared > Real::zero() {
            let sin_theta = sin_squared.sqrt();
            *a_angle = two * Real::atan2(sin_theta, self.m_data[0]);
            let k = Real::one() / sin_theta;
            *a_axis.x_mut() = self.m_data[1] * k;
            *a_axis.y_mut() = self.m_data[2] * k;
            *a_axis.z_mut() = self.m_data[3] * k;
            a_axis.normalize();
        } else {
            // For almost zero rotation
            *a_angle = Real::zero();
            *a_axis.x_mut() = Real::one(); // m_data[1] * 2.0;
            *a_axis.y_mut() = Real::zero(); // m_data[2] * 2.0;
            *a_axis.z_mut() = Real::zero(); // m_data[3] * 2.0;
        }
        // Ensure that angle is always in  [-PI...PI] range
        if *a_angle > Real::PI() {
            *a_angle -= two * Real::PI();
        } else if *a_angle < -Real::PI() {
            *a_angle += two * Real::PI();
        }
    }

    /// Set the quaternion from three angles (NASA angle set) heading, bank, and attitude.
    pub fn q_from_nasa_angles(&mut self, ang: &ChVector<Real>) {
        let two = Real::from(2.0).unwrap();
        let c1 = (*ang.z() / two).cos();
        let s1 = (*ang.z() / two).sin();
        let c2 = (*ang.x() / two).cos();
        let s2 = (*ang.x() / two).sin();
        let c3 = (*ang.y() / two).cos();
        let s3 = (*ang.y() / two).sin();

        let c1c2 = c1 * c2;
        let s1s2 = s1 * s2;

        self.m_data[0] = c1c2 * c3 + s1s2 * s3;
        self.m_data[1] = c1c2 * s3 - s1s2 * c3;
        self.m_data[2] = c1 * s2 * c3 + s1 * c2 * s3;
        self.m_data[3] = s1 * c2 * c3 - c1 * s2 * s3;
    }

    /// Convert the quaternion to three angles (NASA angle set) heading, bank and attitude.
    pub fn q_to_nasa_angles(&self) -> ChVector<Real> {
        let two = Real::from(2.0).unwrap();
        let mut nasa = ChVector::new_default();
        let sqw = self.m_data[0] * self.m_data[0];
        let sqx = self.m_data[1] * self.m_data[1];
        let sqy = self.m_data[2] * self.m_data[2];
        let sqz = self.m_data[3] * self.m_data[3];
        // heading
        *nasa.z_mut() = Real::atan2(two * (self.m_data[1] * self.m_data[2] + self.m_data[3] * self.m_data[0]), sqx - sqy - sqz + sqw);
        // bank
        *nasa.y_mut() = Real::atan2(two * (self.m_data[2] * self.m_data[3] + self.m_data[1] * self.m_data[0]), -sqx - sqy + sqz + sqw);
        // attitude
        *nasa.x_mut() = Real::asin(-two * (self.m_data[1] * self.m_data[3] - self.m_data[2] * self.m_data[0]));
        return nasa;
    }

    /// Set the quaternion from three angles (Euler Sequence 123) roll, pitch, and yaw.
    pub fn q_from_euler123(&mut self, ang: &ChVector<Real>) {
        let half = Real::from(0.5).unwrap();
        // Angles {phi;theta;psi} aka {roll;pitch;yaw}
        let t0 = (*ang.z() * half).cos();
        let t1 = (*ang.z() * half).sin();
        let t2 = (*ang.x() * half).cos();
        let t3 = (*ang.x() * half).sin();
        let t4 = (*ang.y() * half).cos();
        let t5 = (*ang.y() * half).sin();

        self.m_data[0] = t0 * t2 * t4 + t1 * t3 * t5;
        self.m_data[1] = t0 * t3 * t4 - t1 * t2 * t5;
        self.m_data[2] = t0 * t2 * t5 + t1 * t3 * t4;
        self.m_data[3] = t1 * t2 * t4 - t0 * t3 * t5;
    }

    /// Convert the quaternion to three angles (Euler Sequence 123) roll, pitch, and yaw.
    pub fn q_to_euler123(&self) -> ChVector<Real> {
        let two = Real::from(2.0).unwrap();
        // Angles {phi;theta;psi} aka {roll;pitch;yaw} rotation XYZ
        let mut euler = ChVector::new_default();
        let sq0 = self.m_data[0] * self.m_data[0];
        let sq1 = self.m_data[1] * self.m_data[1];
        let sq2 = self.m_data[2] * self.m_data[2];
        let sq3 = self.m_data[3] * self.m_data[3];
        // roll
        *euler.x_mut() = Real::atan2(two * (self.m_data[2] * self.m_data[3] + self.m_data[0] * self.m_data[1]), sq3 - sq2 - sq1 + sq0);
        // pitch
        *euler.y_mut() = -Real::asin(two * (self.m_data[1] * self.m_data[3] - self.m_data[0] * self.m_data[2]));
        // yaw
        *euler.z_mut() = Real::atan2(two * (self.m_data[1] * self.m_data[2] + self.m_data[3] * self.m_data[0]), sq1 + sq0 - sq3 - sq2);
        return euler;
    }

    /// Set the quaternion dq/dt. Inputs: the vector of angular speed w specified in absolute coords,
    /// and the rotation expressed as a quaternion q.
    pub fn qdt_from_wabs(&mut self, w: &ChVector<Real>, q: &ChQuaternion<Real>) {
        let qwo = ChQuaternion::new_vec(Real::zero(), w);
        self.cross_assign(&qwo, q);
        self.scale(Real::from(0.5).unwrap()); // {q_dt} = 1/2 {0,w}*{q}
    }

    /// Set the quaternion dq/dt. Inputs: the vector of angular speed w specified in relative coords,
    /// and the rotation expressed as a quaternion q.
    pub fn qdt_from_wrel(&mut self, w: &ChVector<Real>, q: &ChQuaternion<Real>) {
        let qwl = ChQuaternion::new_vec(Real::zero(), w);
        self.cross_assign(q, &qwl);
        self.scale(Real::from(0.5).unwrap()); // {q_dt} = 1/2 {q}*{0,w_rel}
    }

    /// Compute the vector of angular speed 'w' specified in absolute coords,
    /// from the quaternion dq/dt and the rotation expressed as a quaternion q.
    pub fn qdt_to_wabs(&self, w: &mut ChVector<Real>, q: &ChQuaternion<Real>) {
        let mut qwo = ChQuaternion::new_default();
        qwo.cross_assign(self, &q.get_conjugate());
        *w = qwo.get_vector();
        w.scale(Real::from(2.0).unwrap());
    }

    /// Compute the vector of angular speed 'w' specified in relative coords,
    /// from the quaternion dq/dt and the rotation expressed as a quaternion q.
    pub fn qdt_to_wrel(&self, w: &mut ChVector<Real>, q: &ChQuaternion<Real>) {
        let mut qwl = ChQuaternion::new_default();
        qwl.cross_assign(&q.get_conjugate(), self);
        *w = qwl.get_vector();
        w.scale(Real::from(2.0).unwrap());
    }

    /// Set the quaternion ddq/dtdt. Inputs: the vector of angular acceleration 'a' specified
    /// in absolute coords, the rotation expressed as a quaternion q, the rotation speed
    /// as a quaternion 'q_dt'.
    pub fn qdtdt_from_aabs(&mut self, a: &ChVector<Real>, q: &ChQuaternion<Real>, q_dt: &ChQuaternion<Real>) {
        let qao = ChQuaternion::new_vec(Real::zero(), a);
        let mut qwo = ChQuaternion::new_default();
        let mut qtmpa = ChQuaternion::new_default();
        let mut qtmpb = ChQuaternion::new_default();
        qwo.cross_assign(q_dt, &q.get_conjugate());
        qtmpb.cross_assign(&qwo, q_dt);
        qtmpa.cross_assign(&qao, q);
        qtmpa.scale(Real::from(0.5).unwrap());
        *self = qtmpa + qtmpb;
    }

    /// Set the quaternion ddq/dtdt. Inputs: the vector of angular acceleration 'a' specified
    /// in relative coords, the rotation expressed as a quaternion q, the rotation speed
    /// as a quaternion 'q_dt'.
    pub fn qdtdt_from_arel(&mut self, a: &ChVector<Real>, q: &ChQuaternion<Real>, q_dt: &ChQuaternion<Real>) {
        let qal = ChQuaternion::new_vec(Real::zero(), a);
        let mut qwl = ChQuaternion::new_default();
        let mut qtmpa = ChQuaternion::new_default();
        let mut qtmpb = ChQuaternion::new_default();
        qwl.cross_assign(&q.get_conjugate(), q_dt);
        qtmpb.cross_assign(q_dt, &qwl);
        qtmpa.cross_assign(q, &qal);
        qtmpa.scale(Real::from(0.5).unwrap());
        *self = qtmpa + qtmpb;
    }

    /// Set the quaternion dq/dt. Inputs:  the axis of rotation 'axis' (assuming it is already normalized
    /// and expressed in absolute coords), the angular speed 'angle_dt' (scalar value), and the
    /// rotation expressed as a quaternion 'q'.
    pub fn qdt_from_ang_axis(&mut self, q: &ChQuaternion<Real>, angle_dt: Real, axis: &ChVector<Real>) {
        self.qdt_from_wabs(&(axis * angle_dt), q);
    }

    /// Set the quaternion ddq/dtdt. Inputs: the axis of ang. acceleration 'axis' (assuming it is already
    /// normalized and expressed in absolute coords), the angular acceleration 'angle_dtdt' (scalar value),
    /// the rotation expressed as a quaternion 'quat' and th rotation speed 'q_dt'.
    pub fn qdtdt_from_ang_axis(&mut self, q: &ChQuaternion<Real>, q_dt: &ChQuaternion<Real>, angle_dtdt: Real,
                               axis: &ChVector<Real>) {
        self.qdtdt_from_aabs(&(axis * angle_dtdt), q, q_dt);
    }

    /// Given the imaginary (vectorial) {e1 e2 e3} part of a quaternion,
    /// tries to set the entire quaternion q = {e0, e1, e2, e3}.  Also for q_dt and q_dtdt.
    /// Note: singularities may happen!
    pub fn imm_q_complete(&mut self, qimm: &ChVector<Real>) {
        self.set_vector(qimm);
        self.m_data[0] = (Real::one() - self.m_data[1] * self.m_data[1] - self.m_data[2] * self.m_data[2] - self.m_data[3] * self.m_data[3]).sqrt();
    }
    pub fn imm_q_dt_complete(&mut self, q: &ChQuaternion<Real>, qimm_dt: &ChVector<Real>) {
        self.set_vector(qimm_dt);
        self.m_data[0] = (-q.m_data[1] * self.m_data[1] - q.m_data[2] * self.m_data[2] - q.m_data[3] * self.m_data[3]) / q.m_data[0];
    }
    pub fn imm_q_dtdt_complete(&mut self, q: &ChQuaternion<Real>, qdt: &ChQuaternion<Real>, qimm_dtdt: &ChVector<Real>) {
        self.set_vector(qimm_dtdt);
        self.m_data[0] =
            (-q.m_data[1] * self.m_data[1] - q.m_data[2] * self.m_data[2] - q.m_data[3] * self.m_data[3] - qdt.m_data[0] * qdt.m_data[0] -
                qdt.m_data[1] * qdt.m_data[1] - qdt.m_data[2] * qdt.m_data[2] - qdt.m_data[3] * qdt.m_data[3]) /
                q.m_data[0];
    }
}

// -------------------------------------------------------------------------------------------------
impl<Real: Float> PartialEq for ChQuaternion<Real> {
    fn eq(&self, other: &Self) -> bool {
        return other.m_data[0] == self.m_data[0] && other.m_data[1] == self.m_data[1]
            && other.m_data[2] == self.m_data[2] && other.m_data[3] == self.m_data[3];
    }
}

impl<Real: Float> Eq for ChQuaternion<Real> {}

impl<Real: Float + Ord> Ord for ChQuaternion<Real> {
    fn cmp(&self, other: &Self) -> Ordering {
        return self.m_data.cmp(&other.m_data);
    }
}

impl<Real: Float + Ord> PartialOrd for ChQuaternion<Real> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        return Some(self.cmp(other));
    }
}

/// # STATIC QUATERNION MATH OPERATIONS
impl<Real: Float + FloatConst + MulAssign + SubAssign + AddAssign + Display> ChQuaternion<Real> {
    /// Get the quaternion from a source vector and a destination vector which specifies
    /// the rotation from one to the other.  The vectors do not need to be normalized.
    pub fn q_from_vect_to_vect(fr_vect: &ChVector<Real>, to_vect: &ChVector<Real>) -> ChQuaternion<Real> {
        let angle_tolerance = Real::from(1e-6).unwrap();
        let mut quat = ChQuaternion::new_default();
        let halfang;
        let sinhalf;
        let mut axis;

        let len_xlen = fr_vect.length() * to_vect.length();
        axis = fr_vect % to_vect;
        let sinangle = ch_clamp(axis.length() / len_xlen, -Real::one(), Real::one());
        let cosangle = ch_clamp(fr_vect ^ to_vect / len_xlen, -Real::one(), Real::one());

        // Consider three cases: Parallel, Opposite, non-collinear
        if sinangle.abs() == Real::zero() && cosangle > Real::zero() {
            // fr_vect & to_vect are parallel
            *quat.e0_mut() = Real::one();
            *quat.e1_mut() = Real::zero();
            *quat.e2_mut() = Real::zero();
            *quat.e3_mut() = Real::zero();
        } else if sinangle.abs() < angle_tolerance && cosangle < Real::zero() {
            // fr_vect & to_vect are opposite, i.e. ~180 deg apart
            axis = fr_vect.get_orthogonal_vector() + (-to_vect).get_orthogonal_vector();
            axis.normalize();
            *quat.e0_mut() = Real::zero();
            *quat.e1_mut() = ch_clamp(*axis.x(), -Real::one(), Real::one());
            *quat.e2_mut() = ch_clamp(*axis.y(), -Real::one(), Real::one());
            *quat.e3_mut() = ch_clamp(*axis.z(), -Real::one(), Real::one());
        } else {
            // fr_vect & to_vect are not co-linear case
            axis.normalize();
            halfang = Real::from(0.5).unwrap() * ch_atan2(cosangle, sinangle);
            sinhalf = halfang.sin();

            *quat.e0_mut() = halfang.cos();
            *quat.e1_mut() = sinhalf * *axis.x();
            *quat.e2_mut() = sinhalf * *axis.y();
            *quat.e3_mut() = sinhalf * *axis.z();
        }
        return quat;
    }

    // Get the X axis of a coordinate system, given the quaternion which
    // represents the alignment of the coordinate system.
    pub fn v_axis_x_from_quat(quat: &ChQuaternion<Real>) -> ChVector<Real> {
        let two = Real::from(2.0).unwrap();
        let mut res = ChVector::new_default();
        *res.x_mut() = (Real::powi(*quat.e0(), 2) + Real::powi(*quat.e1(), 2)) * two - Real::one();
        *res.y_mut() = ((*quat.e1() * *quat.e2()) + (*quat.e0() * *quat.e3())) * two;
        *res.z_mut() = ((*quat.e1() * *quat.e3()) - (*quat.e0() * *quat.e2())) * two;
        return res;
    }

    /// Angle conversion utilities.
    pub fn quat_to_angle(angset: AngleSet, mquat: &ChQuaternion<Real>) -> ChVector<Real> {
        todo!()
    }
    pub fn angle_to_angle(setfrom: AngleSet, setto: AngleSet, mangles: &ChVector<Real>) -> ChVector<Real> {
        todo!()
    }
    pub fn angle_to_quat(angset: AngleSet, mangles: &ChVector<Real>) -> ChQuaternion<Real> {
        todo!()
    }
    pub fn angle_dt_to_quat_dt(angset: AngleSet, mangles: &ChVector<Real>, q: &ChQuaternion<Real>) -> ChQuaternion<Real> {
        let ang1 = ChQuaternion::quat_to_angle(angset.clone(), q);
        let ang2 = ang1 + mangles * Real::from(BDF_STEP_HIGH).unwrap();
        let q2 = ChQuaternion::angle_to_quat(angset, &ang2);
        let res = (q2 - q) * Real::from(1.0 / BDF_STEP_HIGH).unwrap();

        return res;
    }
    pub fn angle_dtdt_to_quat_dtdt(angset: AngleSet, mangles: &ChVector<Real>, q: &ChQuaternion<Real>) -> ChQuaternion<Real> {
        let ang0 = ChQuaternion::quat_to_angle(angset.clone(), q);
        let ang_a = ang0.clone() - mangles * Real::from(BDF_STEP_HIGH).unwrap();
        let ang_b = ang0.clone() + mangles * Real::from(BDF_STEP_HIGH).unwrap();
        let qa = ChQuaternion::angle_to_quat(angset.clone(), &ang_a);
        let qb = ChQuaternion::angle_to_quat(angset, &ang_b);
        let res = (qa + qb + q * Real::from(-2.0).unwrap()) * Real::from(1.0 / BDF_STEP_HIGH).unwrap();

        return res;
    }
}

// -------------------------------------------------------------------------------------------------

/// Shortcut for faster use of typical double-precision quaternion.
/// <pre>
/// Instead of writing
///    ChQuaternion<double> foo;
/// or
///    ChQuaternion<> foo;
/// you can use the shorter version
///    Quaternion foo;
/// </pre>
pub type Quaternion = ChQuaternion<f64>;

/// Shortcut for faster use of typical single-precision quaternion.
/// <pre>
/// Instead of writing
///    ChQuaternion<float> foo;
/// you can use the shorter version
///    Quaternion foo;
/// </pre>
pub type QuaternionF = ChQuaternion<f32>;

// -------------------------------------------------------------------------------------------------
// CONSTANTS

/// Constant null quaternion: {0, 0, 0, 0}
pub const QNULL: ChQuaternion<f64> = ChQuaternion { m_data: [0.0; 4] };

/// Constant unit quaternion: {1, 0, 0, 0} ,
/// corresponds to no rotation (diagonal rotation matrix)
pub const QUNIT: ChQuaternion<f64> = ChQuaternion { m_data: [1.0, 0.0, 0.0, 0.0] };

// Constants for rotations of 90 degrees:
pub const Q_ROTATE_Y_TO_X: ChQuaternion<f64> = ChQuaternion { m_data: [CH_C_SQRT_1_2, 0.0, 0.0, -CH_C_SQRT_1_2] };
pub const Q_ROTATE_Y_TO_Z: ChQuaternion<f64> = ChQuaternion { m_data: [CH_C_SQRT_1_2, CH_C_SQRT_1_2, 0.0, 0.0] };
pub const Q_ROTATE_X_TO_Y: ChQuaternion<f64> = ChQuaternion { m_data: [CH_C_SQRT_1_2, 0.0, 0.0, CH_C_SQRT_1_2] };
pub const Q_ROTATE_X_TO_Z: ChQuaternion<f64> = ChQuaternion { m_data: [CH_C_SQRT_1_2, 0.0, -CH_C_SQRT_1_2, 0.0] };
pub const Q_ROTATE_Z_TO_Y: ChQuaternion<f64> = ChQuaternion { m_data: [CH_C_SQRT_1_2, -CH_C_SQRT_1_2, 0.0, 0.0] };
pub const Q_ROTATE_Z_TO_X: ChQuaternion<f64> = ChQuaternion { m_data: [CH_C_SQRT_1_2, 0.0, CH_C_SQRT_1_2, 0.0] };

// Constants for rotations of 180 degrees:
pub const Q_FLIP_AROUND_X: ChQuaternion<f64> = ChQuaternion { m_data: [0.0, 1.0, 0.0, 0.0] };
pub const Q_FLIP_AROUND_Y: ChQuaternion<f64> = ChQuaternion { m_data: [0.0, 0.0, 1.0, 0.0] };
pub const Q_FLIP_AROUND_Z: ChQuaternion<f64> = ChQuaternion { m_data: [0.0, 0.0, 0.0, 1.0] };
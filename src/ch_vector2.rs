/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use std::ops::*;
use std::cmp::Ordering;

/// Definition of a general purpose 2d vector.
/// ChVector2 is templated by precision, with default 'double'.
#[derive(Clone)]
pub struct ChVector2<Real: Float> {
    pub(crate) m_data: [Real; 2],
}

impl<Real: Float> ChVector2<Real> {
    pub fn new_default() -> ChVector2<Real> {
        return ChVector2 {
            m_data: [Real::zero(), Real::zero()]
        };
    }

    pub fn new(x: Real, y: Real) -> ChVector2<Real> {
        return ChVector2 {
            m_data: [x, y]
        };
    }

    pub fn new_scalar(a: Real) -> ChVector2<Real> {
        return ChVector2 {
            m_data: [a, a]
        };
    }

    /// Access to components
    pub fn x(&self) -> &Real { return &self.m_data[0]; }
    pub fn y(&self) -> &Real { return &self.m_data[1]; }
    pub fn x_mut(&mut self) -> &Real { return &mut self.m_data[0]; }
    pub fn y_mut(&mut self) -> &Real { return &mut self.m_data[1]; }

    /// Return const pointer to underlying array storage.
    pub fn data(&self) -> &[Real] { return &self.m_data; }

    // SET FUNCTIONS

    /// set the two values of the vector at once.
    pub fn set(&mut self, x: Real, y: Real) {
        self.m_data[0] = x;
        self.m_data[1] = y;
    }

    /// Sets the vector as a copy of another vector.
    pub fn set_vec(&mut self, v: &ChVector2<Real>) {
        self.m_data[0] = v.m_data[0];
        self.m_data[1] = v.m_data[1];
    }

    /// set all the vector components ts to the same scalar.
    pub fn set_scalar(&mut self, s: Real) {
        self.m_data[0] = s;
        self.m_data[1] = s;
    }

    /// set the vector to the null vector.
    pub fn set_null(&mut self) {
        self.m_data[0] = Real::zero();
        self.m_data[1] = Real::zero();
    }

    /// Return true if this vector is the null vector.
    pub fn is_null(&self) -> bool {
        return self.m_data[0] == Real::zero() && self.m_data[1] == Real::zero();
    }

    /// Return true if this vector is equal to another vector.
    pub fn equals(&self, other: &ChVector2<Real>) -> bool {
        return (other.m_data[0] == self.m_data[0]) && (other.m_data[1] == self.m_data[1]);
    }

    /// Return true if this vector is equal to another vector, within a tolerance 'tol'.
    pub fn equals_tol(&self, other: &ChVector2<Real>, tol: Real) -> bool {
        return (other.m_data[0] - self.m_data[0]).abs() < tol && (other.m_data[1] - self.m_data[1]).abs() < tol;
    }

    // VECTOR NORMS

    /// Compute the euclidean norm of the vector, that is its length or magnitude.
    pub fn length(&self) -> Real {
        return self.length2().sqrt();
    }

    /// Compute the squared euclidean norm of the vector.
    pub fn length2(&self) -> Real {
        todo!()
    }

    /// Compute the infinite norm of the vector,
    /// that is the maximum absolute value of one of its elements.
    pub fn length_inf(&self) -> Real {
        return Real::max(self.m_data[0].abs(), self.m_data[1].abs());
    }
}

//--------------------------------------------------------------------------------------------------
impl<Real: Float> Index<usize> for ChVector2<Real> {
    type Output = Real;

    fn index(&self, index: usize) -> &Self::Output {
        return &self.m_data[index];
    }
}

impl<Real: Float> IndexMut<usize> for ChVector2<Real> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self.m_data[index];
    }
}

impl<Real: Float> Neg for ChVector2<Real> {
    type Output = ChVector2<Real>;

    fn neg(self) -> Self::Output {
        return ChVector2::new(-self.m_data[0], -self.m_data[1]);
    }
}

impl<Real: Float> Neg for &ChVector2<Real> {
    type Output = ChVector2<Real>;

    fn neg(self) -> Self::Output {
        return ChVector2::new(-self.m_data[0], -self.m_data[1]);
    }
}

macro_rules! impl_add {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float> Add<$OTHER> for $SELF {
            type Output = ChVector2<Real>;

            fn add(self, rhs: $OTHER) -> Self::Output {
                return ChVector2::new(self.m_data[0] + rhs.m_data[0],
                                     self.m_data[1] + rhs.m_data[1]);
            }
        }
    };
}
impl_add!(ChVector2<Real>, ChVector2<Real>);
impl_add!(ChVector2<Real>, &ChVector2<Real>);
impl_add!(&ChVector2<Real>, ChVector2<Real>);
impl_add!(&ChVector2<Real>, &ChVector2<Real>);

macro_rules! impl_sub {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float> Sub<$OTHER> for $SELF {
            type Output = ChVector2<Real>;

            fn sub(self, rhs: $OTHER) -> Self::Output {
                return ChVector2::new(self.m_data[0] - rhs.m_data[0],
                                     self.m_data[1] - rhs.m_data[1]);
            }
        }
    };
}
impl_sub!(ChVector2<Real>, ChVector2<Real>);
impl_sub!(ChVector2<Real>, &ChVector2<Real>);
impl_sub!(&ChVector2<Real>, ChVector2<Real>);
impl_sub!(&ChVector2<Real>, &ChVector2<Real>);

macro_rules! impl_mul {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real:Float> Mul<$OTHER> for $SELF {
            type Output = ChVector2<Real>;

            fn mul(self, rhs: $OTHER) -> Self::Output {
                return ChVector2::new(self.m_data[0] * rhs.m_data[0],
                                     self.m_data[1] * rhs.m_data[1]);
            }
        }
    };
}
impl_mul!(ChVector2<Real>, ChVector2<Real>);
impl_mul!(ChVector2<Real>, &ChVector2<Real>);
impl_mul!(&ChVector2<Real>, ChVector2<Real>);
impl_mul!(&ChVector2<Real>, &ChVector2<Real>);

macro_rules! impl_div {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real:Float> Div<$OTHER> for $SELF {
            type Output = ChVector2<Real>;

            fn div(self, rhs: $OTHER) -> Self::Output {
                return ChVector2::new(self.m_data[0] / rhs.m_data[0],
                                     self.m_data[1] / rhs.m_data[1]);
            }
        }
    };
}
impl_div!(ChVector2<Real>, ChVector2<Real>);
impl_div!(ChVector2<Real>, &ChVector2<Real>);
impl_div!(&ChVector2<Real>, ChVector2<Real>);
impl_div!(&ChVector2<Real>, &ChVector2<Real>);

macro_rules! impl_mul_scalar {
    ($SELF:ty) => {
        impl<Real:Float> Mul<Real> for $SELF {
            type Output = ChVector2<Real>;

            fn mul(self, rhs: Real) -> Self::Output {
                return ChVector2::new(self.m_data[0] * rhs,
                                     self.m_data[1] * rhs);
            }
        }
    };
}
impl_mul_scalar!(ChVector2<Real>);
impl_mul_scalar!(&ChVector2<Real>);

macro_rules! impl_div_scalar {
    ($SELF:ty) => {
        impl<Real:Float> Div<Real> for $SELF {
            type Output = ChVector2<Real>;

            fn div(self, rhs: Real) -> Self::Output {
                return ChVector2::new(self.m_data[0] / rhs,
                                     self.m_data[1] / rhs);
            }
        }
    };
}
impl_div_scalar!(ChVector2<Real>);
impl_div_scalar!(&ChVector2<Real>);

macro_rules! impl_add_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + AddAssign> AddAssign<$OTHER> for $SELF {
            fn add_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] += rhs.m_data[0];
                self.m_data[1] += rhs.m_data[1];
            }
        }
    };
}
impl_add_assign!(ChVector2<Real>, ChVector2<Real>);
impl_add_assign!(ChVector2<Real>, &ChVector2<Real>);

macro_rules! impl_sub_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + SubAssign> SubAssign<$OTHER> for $SELF {
            fn sub_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] -= rhs.m_data[0];
                self.m_data[1] -= rhs.m_data[1];
            }
        }
    };
}
impl_sub_assign!(ChVector2<Real>, ChVector2<Real>);
impl_sub_assign!(ChVector2<Real>, &ChVector2<Real>);

macro_rules! impl_mul_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + MulAssign> MulAssign<$OTHER> for $SELF {
            fn mul_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] *= rhs.m_data[0];
                self.m_data[1] *= rhs.m_data[1];
            }
        }
    };
}
impl_mul_assign!(ChVector2<Real>, ChVector2<Real>);
impl_mul_assign!(ChVector2<Real>, &ChVector2<Real>);

macro_rules! impl_div_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + DivAssign> DivAssign<$OTHER> for $SELF {
            fn div_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] /= rhs.m_data[0];
                self.m_data[1] /= rhs.m_data[1];
            }
        }
    };
}
impl_div_assign!(ChVector2<Real>, ChVector2<Real>);
impl_div_assign!(ChVector2<Real>, &ChVector2<Real>);

impl<Real: Float + MulAssign> MulAssign<Real> for ChVector2<Real> {
    fn mul_assign(&mut self, rhs: Real) {
        self.m_data[0] *= rhs;
        self.m_data[1] *= rhs;
    }
}

impl<Real: Float + DivAssign> DivAssign<Real> for ChVector2<Real> {
    fn div_assign(&mut self, rhs: Real) {
        self.m_data[0] /= rhs;
        self.m_data[1] /= rhs;
    }
}

//--------------------------------------------------------------------------------------------------
impl<Real: Float> ChVector2<Real> {
    /// Return the dot product with another vector: result = this ^ b
    pub fn dot(&self, b: &ChVector2<Real>) -> Real {
        return (self.m_data[0] * b.m_data[0]) + (self.m_data[1] * b.m_data[1]);
    }
}

impl<Real: Float + MulAssign> ChVector2<Real> {
    /// scale this vector by a scalar: this *= s
    pub fn scale(&mut self, s: Real) {
        self.m_data[0] *= s;
        self.m_data[1] *= s;
    }

    /// normalize this vector in place, so that its euclidean length is 1.
    /// Return false if the original vector had zero length (in which case the vector
    /// is set to [1,0,0]) and return true otherwise.
    pub fn normalize(&mut self) -> bool {
        let length = self.length();
        if length < Real::min_positive_value() {
            self.m_data[0] = Real::one();
            self.m_data[1] = Real::zero();
            return false;
        }
        self.scale(Real::one() / length);
        return true;
    }

    /// Return a normalized copy of this vector, with euclidean length = 1.
    /// Not to be confused with normalize() which normalizes in place.
    pub fn get_normalized(&self) -> ChVector2<Real> {
        let mut v = self.clone();
        v.normalize();
        return v;
    }

    /// Impose a new length to the vector, keeping the direction unchanged.
    pub fn set_length(&mut self, s: Real) {
        self.normalize();
        self.scale(s);
    }

    /// Apply a 2D rotation of given angle (positive counterclockwise).
    pub fn rotate(&mut self, angle:Real) {
        let ca = Real::cos(angle);
        let sa = Real::sin(angle);
        let tmp = self.m_data[0] * ca - self.m_data[1] * sa;
        self.m_data[1] = self.m_data[0] * sa + self.m_data[1] * ca;
        self.m_data[0] = tmp;
    }

    /// Return the index of the largest component in absolute value.
    pub fn get_max_component(&self) -> usize {
        let mut idx = 0;
        let max = Real::abs(self.m_data[0]);
        if Real::abs(self.m_data[1]) > max {
            idx = 1;
        }
        return idx;
    }

    /// Return a unit vector orthogonal to this vector
    pub fn get_orthogonal_vector(&self) ->ChVector2<Real> {
        let mut ortho = ChVector2::new(-self.m_data[1], self.m_data[0]);
        ortho.normalize();
        return ortho;
    }
}

//--------------------------------------------------------------------------------------------------
impl<Real: Float> BitXor for ChVector2<Real> {
    type Output = Real;

    fn bitxor(self, rhs: Self) -> Self::Output {
        return self.dot(&rhs);
    }
}

impl<Real: Float> PartialEq for ChVector2<Real> {
    fn eq(&self, other: &Self) -> bool {
        return other.m_data[0] == self.m_data[0] && other.m_data[1] == self.m_data[1];
    }
}

impl<Real: Float> Eq for ChVector2<Real> {}

impl<Real: Float + Ord> Ord for ChVector2<Real> {
    fn cmp(&self, other: &Self) -> Ordering {
        return self.m_data.cmp(&other.m_data);
    }
}

impl<Real: Float + Ord> PartialOrd for ChVector2<Real> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        return Some(self.cmp(other));
    }
}
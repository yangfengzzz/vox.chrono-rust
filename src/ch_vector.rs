/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use num::Float;
use std::ops::*;
use std::cmp::Ordering;
use std::fmt::Display;

/// Definition of general purpose 3d vector variables, such as points in 3D.
/// This class implements the vector algebra in 3D (Gibbs products).
/// ChVector is templated by precision, with default 'double'.
///
/// Further info at the @ref mathematical_objects manual page.
#[derive(Clone)]
pub struct ChVector<Real: Float> {
    pub(crate) m_data: [Real; 3],
}

impl<Real: Float> ChVector<Real> {
    pub fn new_default() -> ChVector<Real> {
        return ChVector {
            m_data: [Real::zero(), Real::zero(), Real::zero()]
        };
    }

    pub fn new(x: Real, y: Real, z: Real) -> ChVector<Real> {
        return ChVector {
            m_data: [x, y, z]
        };
    }

    pub fn new_scalar(a: Real) -> ChVector<Real> {
        return ChVector {
            m_data: [a, a, a]
        };
    }

    /// Access to components
    pub fn x(&self) -> &Real { return &self.m_data[0]; }
    pub fn y(&self) -> &Real { return &self.m_data[1]; }
    pub fn z(&self) -> &Real { return &self.m_data[2]; }
    pub fn x_mut(&mut self) -> &mut Real { return &mut self.m_data[0]; }
    pub fn y_mut(&mut self) -> &mut Real { return &mut self.m_data[1]; }
    pub fn z_mut(&mut self) -> &mut Real { return &mut self.m_data[2]; }

    /// Return const pointer to underlying array storage.
    pub fn data(&self) -> &[Real] { return &self.m_data; }
}

impl<Real: Float> ChVector<Real> {
    // SET FUNCTIONS

    /// set the three values of the vector at once.
    pub fn set(&mut self, x: Real, y: Real, z: Real) {
        self.m_data[0] = x;
        self.m_data[1] = y;
        self.m_data[2] = z;
    }

    /// set the vector as a copy of another vector.
    pub fn set_vec(&mut self, v: &ChVector<Real>) {
        self.m_data[0] = v.m_data[0];
        self.m_data[1] = v.m_data[1];
        self.m_data[2] = v.m_data[2];
    }

    /// set all the vector components ts to the same scalar.
    pub fn set_scalar(&mut self, s: Real) {
        self.m_data[0] = s;
        self.m_data[1] = s;
        self.m_data[2] = s;
    }

    /// set the vector to the null vector.
    pub fn set_null(&mut self) {
        self.m_data[0] = Real::zero();
        self.m_data[1] = Real::zero();
        self.m_data[2] = Real::zero();
    }

    /// Return true if this vector is the null vector.
    pub fn is_null(&self) -> bool {
        return self.m_data[0] == Real::zero() && self.m_data[1] == Real::zero() && self.m_data[2] == Real::zero();
    }

    /// Return true if this vector is equal to another vector.
    pub fn equals(&self, other: &ChVector<Real>) -> bool {
        return (other.m_data[0] == self.m_data[0]) && (other.m_data[1] == self.m_data[1]) && (other.m_data[2] == self.m_data[2]);
    }

    /// Return true if this vector is equal to another vector, within a tolerance 'tol'.
    pub fn equals_tol(&self, other: &ChVector<Real>, tol: Real) -> bool {
        return (Real::abs(other.m_data[0] - self.m_data[0]) < tol) && (Real::abs(other.m_data[1] - self.m_data[1]) < tol) &&
            (Real::abs(other.m_data[2] - self.m_data[2]) < tol);
    }

    // VECTOR NORMS

    /// Compute the euclidean norm of the vector, that is its length or magnitude.
    pub fn length(&self) -> Real {
        return self.length2().sqrt();
    }

    /// Compute the squared euclidean norm of the vector.
    pub fn length2(&self) -> Real {
        return self.dot(self);
    }

    /// Compute the infinity norm of the vector, that is the maximum absolute value of one of its elements.
    pub fn length_inf(&self) -> Real {
        return Real::max(Real::max(Real::abs(self.m_data[0]),
                                   Real::abs(self.m_data[1])),
                         Real::abs(self.m_data[2]));
    }
}

//--------------------------------------------------------------------------------------------------
impl<Real: Float> Index<usize> for ChVector<Real> {
    type Output = Real;

    fn index(&self, index: usize) -> &Self::Output {
        return &self.m_data[index];
    }
}

impl<Real: Float> IndexMut<usize> for ChVector<Real> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        return &mut self.m_data[index];
    }
}

impl<Real: Float> Neg for ChVector<Real> {
    type Output = ChVector<Real>;

    fn neg(self) -> Self::Output {
        return ChVector::new(-self.m_data[0], -self.m_data[1], -self.m_data[2]);
    }
}

impl<Real: Float> Neg for &ChVector<Real> {
    type Output = ChVector<Real>;

    fn neg(self) -> Self::Output {
        return ChVector::new(-self.m_data[0], -self.m_data[1], -self.m_data[2]);
    }
}

macro_rules! impl_add {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float> Add<$OTHER> for $SELF {
            type Output = ChVector<Real>;

            fn add(self, rhs: $OTHER) -> Self::Output {
                return ChVector::new(self.m_data[0] + rhs.m_data[0],
                                     self.m_data[1] + rhs.m_data[1],
                                     self.m_data[2] + rhs.m_data[2]);
            }
        }
    };
}
impl_add!(ChVector<Real>, ChVector<Real>);
impl_add!(ChVector<Real>, &ChVector<Real>);
impl_add!(&ChVector<Real>, ChVector<Real>);
impl_add!(&ChVector<Real>, &ChVector<Real>);

macro_rules! impl_sub {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float> Sub<$OTHER> for $SELF {
            type Output = ChVector<Real>;

            fn sub(self, rhs: $OTHER) -> Self::Output {
                return ChVector::new(self.m_data[0] - rhs.m_data[0],
                                     self.m_data[1] - rhs.m_data[1],
                                     self.m_data[2] - rhs.m_data[2]);
            }
        }
    };
}
impl_sub!(ChVector<Real>, ChVector<Real>);
impl_sub!(ChVector<Real>, &ChVector<Real>);
impl_sub!(&ChVector<Real>, ChVector<Real>);
impl_sub!(&ChVector<Real>, &ChVector<Real>);

macro_rules! impl_mul {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real:Float> Mul<$OTHER> for $SELF {
            type Output = ChVector<Real>;

            fn mul(self, rhs: $OTHER) -> Self::Output {
                return ChVector::new(self.m_data[0] * rhs.m_data[0],
                                     self.m_data[1] * rhs.m_data[1],
                                     self.m_data[2] * rhs.m_data[2]);
            }
        }
    };
}
impl_mul!(ChVector<Real>, ChVector<Real>);
impl_mul!(ChVector<Real>, &ChVector<Real>);
impl_mul!(&ChVector<Real>, ChVector<Real>);
impl_mul!(&ChVector<Real>, &ChVector<Real>);

macro_rules! impl_div {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real:Float> Div<$OTHER> for $SELF {
            type Output = ChVector<Real>;

            fn div(self, rhs: $OTHER) -> Self::Output {
                return ChVector::new(self.m_data[0] / rhs.m_data[0],
                                     self.m_data[1] / rhs.m_data[1],
                                     self.m_data[2] / rhs.m_data[2]);
            }
        }
    };
}
impl_div!(ChVector<Real>, ChVector<Real>);
impl_div!(ChVector<Real>, &ChVector<Real>);
impl_div!(&ChVector<Real>, ChVector<Real>);
impl_div!(&ChVector<Real>, &ChVector<Real>);

macro_rules! impl_mul_scalar {
    ($SELF:ty) => {
        impl<Real:Float> Mul<Real> for $SELF {
            type Output = ChVector<Real>;

            fn mul(self, rhs: Real) -> Self::Output {
                return ChVector::new(self.m_data[0] * rhs,
                                     self.m_data[1] * rhs,
                                     self.m_data[2] * rhs);
            }
        }
    };
}
impl_mul_scalar!(ChVector<Real>);
impl_mul_scalar!(&ChVector<Real>);

macro_rules! impl_div_scalar {
    ($SELF:ty) => {
        impl<Real:Float> Div<Real> for $SELF {
            type Output = ChVector<Real>;

            fn div(self, rhs: Real) -> Self::Output {
                return ChVector::new(self.m_data[0] / rhs,
                                     self.m_data[1] / rhs,
                                     self.m_data[2] / rhs);
            }
        }
    };
}
impl_div_scalar!(ChVector<Real>);
impl_div_scalar!(&ChVector<Real>);

macro_rules! impl_add_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + AddAssign> AddAssign<$OTHER> for $SELF {
            fn add_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] += rhs.m_data[0];
                self.m_data[1] += rhs.m_data[1];
                self.m_data[2] += rhs.m_data[2];
            }
        }
    };
}
impl_add_assign!(ChVector<Real>, ChVector<Real>);
impl_add_assign!(ChVector<Real>, &ChVector<Real>);

macro_rules! impl_sub_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + SubAssign> SubAssign<$OTHER> for $SELF {
            fn sub_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] -= rhs.m_data[0];
                self.m_data[1] -= rhs.m_data[1];
                self.m_data[2] -= rhs.m_data[2];
            }
        }
    };
}
impl_sub_assign!(ChVector<Real>, ChVector<Real>);
impl_sub_assign!(ChVector<Real>, &ChVector<Real>);

macro_rules! impl_mul_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + MulAssign> MulAssign<$OTHER> for $SELF {
            fn mul_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] *= rhs.m_data[0];
                self.m_data[1] *= rhs.m_data[1];
                self.m_data[2] *= rhs.m_data[2];
            }
        }
    };
}
impl_mul_assign!(ChVector<Real>, ChVector<Real>);
impl_mul_assign!(ChVector<Real>, &ChVector<Real>);

macro_rules! impl_div_assign {
    ($SELF:ty, $OTHER:ty ) => {
        impl<Real: Float + DivAssign> DivAssign<$OTHER> for $SELF {
            fn div_assign(&mut self, rhs: $OTHER) {
                self.m_data[0] /= rhs.m_data[0];
                self.m_data[1] /= rhs.m_data[1];
                self.m_data[2] /= rhs.m_data[2];
            }
        }
    };
}
impl_div_assign!(ChVector<Real>, ChVector<Real>);
impl_div_assign!(ChVector<Real>, &ChVector<Real>);

impl<Real: Float + MulAssign> MulAssign<Real> for ChVector<Real> {
    fn mul_assign(&mut self, rhs: Real) {
        self.m_data[0] *= rhs;
        self.m_data[1] *= rhs;
        self.m_data[2] *= rhs;
    }
}

impl<Real: Float + DivAssign> DivAssign<Real> for ChVector<Real> {
    fn div_assign(&mut self, rhs: Real) {
        self.m_data[0] /= rhs;
        self.m_data[1] /= rhs;
        self.m_data[2] /= rhs;
    }
}

//--------------------------------------------------------------------------------------------------
impl<Real: Float> ChVector<Real> {
    /// set this vector to the cross product of a and b: this = a x b
    pub fn cross_assign(&mut self, a: &ChVector<Real>, b: &ChVector<Real>) {
        self.m_data[0] = (a.m_data[1] * b.m_data[2]) - (a.m_data[2] * b.m_data[1]);
        self.m_data[1] = (a.m_data[2] * b.m_data[0]) - (a.m_data[0] * b.m_data[2]);
        self.m_data[2] = (a.m_data[0] * b.m_data[1]) - (a.m_data[1] * b.m_data[0]);
    }

    /// Return the cross product with another vector: result = this x other
    pub fn cross(&self, other: &ChVector<Real>) -> ChVector<Real> {
        let mut v = ChVector::new_default();
        v.cross_assign(self, other);
        return v;
    }

    /// Return the dot product with another vector: result = this ^ b
    pub fn dot(&self, b: &ChVector<Real>) -> Real {
        return (self.m_data[0] * b.m_data[0]) + (self.m_data[1] * b.m_data[1]) + (self.m_data[2] * b.m_data[2]);
    }
}

impl<Real: Float + MulAssign + Display> ChVector<Real> {
    /// scale this vector by a scalar: this *= s
    pub fn scale(&mut self, s: Real) {
        self.m_data[0] *= s;
        self.m_data[1] *= s;
        self.m_data[2] *= s;
    }

    /// normalize this vector in place, so that its euclidean length is 1.
    /// Return false if the original vector had zero length (in which case the vector
    /// is set to [1,0,0]) and return true otherwise.
    pub fn normalize(&mut self) -> bool {
        let length = self.length();
        if length < Real::min_positive_value() {
            self.m_data[0] = Real::one();
            self.m_data[1] = Real::zero();
            self.m_data[2] = Real::zero();
            return false;
        }
        self.scale(Real::one() / length);
        return true;
    }

    /// Return a normalized copy of this vector, with euclidean length = 1.
    /// Not to be confused with normalize() which normalizes in place.
    pub fn get_normalized(&self) -> ChVector<Real> {
        let mut v = self.clone();
        v.normalize();
        return v;
    }

    /// Impose a new length to the vector, keeping the direction unchanged.
    pub fn set_length(&mut self, s: Real) {
        self.normalize();
        self.scale(s);
    }

    /// Use the Gram-Schmidt orthonormalizing to find the three
    /// orthogonal vectors of a coordinate system whose X axis is this vector.
    /// vsingular (optional) sets the normal to the plane on which Dz must lie.
    pub fn dir_to_dx_dy_dz(&self, vx: &mut ChVector<Real>, vy: &mut ChVector<Real>, vz: &mut ChVector<Real>,
                           vsingular: &Option<ChVector<Real>>) {
        // set vx.
        if self.is_null() {
            *vx = ChVector::new(Real::one(), Real::zero(), Real::zero());
        } else {
            *vx = self.get_normalized();
        }

        let default_singular = ChVector::new(Real::zero(), Real::one(), Real::zero());
        let vsingular = vsingular.as_ref().unwrap_or(&default_singular);
        vz.cross_assign(vx, &vsingular);
        let mut zlen = vz.length();

        // if near singularity, change the singularity reference vector.
        if zlen < Real::from(0.0001).unwrap() {
            let mut m_vsingular = ChVector::new_default();

            if Real::abs(vsingular.m_data[0]) < Real::from(0.9).unwrap() {
                m_vsingular = ChVector::new(Real::one(), Real::zero(), Real::zero());
            } else if Real::abs(vsingular.m_data[1]) < Real::from(0.9).unwrap() {
                m_vsingular = ChVector::new(Real::zero(), Real::one(), Real::zero());
            } else if Real::abs(vsingular.m_data[2]) < Real::from(0.9).unwrap() {
                m_vsingular = ChVector::new(Real::zero(), Real::zero(), Real::one());
            }

            vz.cross_assign(vx, &m_vsingular);
            zlen = vz.length(); // now should be nonzero length.
        }

        // normalize vz.
        vz.scale(Real::zero() / zlen);

        // compute vy.
        vy.cross_assign(vz, vx);
    }

    /// Return the index of the largest component in absolute value.
    pub fn get_max_component(&self) -> usize {
        let mut idx = 0;
        let mut max = Real::abs(self.m_data[0]);
        if Real::abs(self.m_data[1]) > max {
            idx = 1;
            max = self.m_data[1];
        }
        if Real::abs(self.m_data[2]) > max {
            idx = 2;
        }
        return idx;
    }

    /// Return a unit vector orthogonal to this vector
    pub fn get_orthogonal_vector(&self) -> ChVector<Real> {
        let idx1 = self.get_max_component();
        let idx2 = (idx1 + 1) % 3; // cycle to the next component
        let idx3 = (idx2 + 1) % 3; // cycle to the next component

        // Construct v2 by rotating in the plane containing the maximum component
        let v2 = ChVector::new(-self.m_data[idx2], self.m_data[idx1], self.m_data[idx3]);

        // Construct the normal vector
        let mut ortho = self.cross(&v2);
        ortho.normalize();
        return ortho;
    }
}

impl<Real: Float + MulAssign + Display> ChVector<Real> {
    // Gets the zenith angle of a unit vector respect to YZ plane  ***OBSOLETE
    pub fn angle_yz_plane(va: &ChVector<Real>) -> Real {
        return Real::asin(va.dot(&ChVector::new(Real::one(), Real::zero(), Real::zero())));
    }

    // Gets the zenith angle of a unit vector respect to YZ plane  ***OBSOLETE
    pub fn angle_yz_plane_norm(va: &ChVector<Real>) -> Real {
        return Real::acos(va.dot(&ChVector::new(Real::one(), Real::zero(), Real::zero())));
    }

    // Gets the angle of the projection on the YZ plane respect to
    // the Y vector, as spinning about X.
    pub fn angle_rx(va: &ChVector<Real>) -> Real {
        let mut vproj = ChVector::new_default();
        *vproj.x_mut() = Real::zero();
        *vproj.y_mut() = *va.y();
        *vproj.z_mut() = *va.z();
        vproj.normalize();
        if *vproj.x() == Real::one() {
            return Real::zero();
        }
        return Real::acos(*vproj.y());
    }

    // The reverse of the two previous functions, gets the vector
    // given the angle above the normal to YZ plane and the angle
    // of rotation on X
    pub fn from_polar(norm_angle: Real, pol_angle: Real) -> ChVector<Real> {
        let mut res = ChVector::new_default();
        *res.x_mut() = Real::cos(norm_angle); // 1) rot 'norm.angle'about z
        *res.y_mut() = Real::sin(norm_angle);
        *res.z_mut() = Real::zero();
        let projlen = *res.y();
        *res.y_mut() = projlen * Real::cos(pol_angle);
        *res.z_mut() = projlen * Real::sin(pol_angle);
        return res;
    }
}

//--------------------------------------------------------------------------------------------------
impl<Real: Float> BitXor<ChVector<Real>> for ChVector<Real> {
    type Output = Real;

    fn bitxor(self, rhs: ChVector<Real>) -> Self::Output {
        return self.dot(&rhs);
    }
}

impl<Real: Float> BitXor<&ChVector<Real>> for ChVector<Real> {
    type Output = Real;

    fn bitxor(self, rhs: &ChVector<Real>) -> Self::Output {
        return self.dot(rhs);
    }
}

impl<Real: Float> BitXor<ChVector<Real>> for &ChVector<Real> {
    type Output = Real;

    fn bitxor(self, rhs: ChVector<Real>) -> Self::Output {
        return self.dot(&rhs);
    }
}

impl<Real: Float> BitXor<&ChVector<Real>> for &ChVector<Real> {
    type Output = Real;

    fn bitxor(self, rhs: &ChVector<Real>) -> Self::Output {
        return self.dot(rhs);
    }
}

impl<Real: Float> Rem<ChVector<Real>> for ChVector<Real> {
    type Output = ChVector<Real>;

    fn rem(self, rhs: ChVector<Real>) -> Self::Output {
        let mut v = ChVector::new_default();
        v.cross_assign(&self, &rhs);
        return v;
    }
}

impl<Real: Float> Rem<ChVector<Real>> for &ChVector<Real> {
    type Output = ChVector<Real>;

    fn rem(self, rhs: ChVector<Real>) -> Self::Output {
        let mut v = ChVector::new_default();
        v.cross_assign(&self, &rhs);
        return v;
    }
}

impl<Real: Float> Rem<&ChVector<Real>> for &ChVector<Real> {
    type Output = ChVector<Real>;

    fn rem(self, rhs: &ChVector<Real>) -> Self::Output {
        let mut v = ChVector::new_default();
        v.cross_assign(&self, rhs);
        return v;
    }
}

impl<Real: Float> RemAssign for ChVector<Real> {
    fn rem_assign(&mut self, rhs: Self) {
        self.cross_assign(&self.clone(), &rhs);
    }
}

impl<Real: Float> PartialEq for ChVector<Real> {
    fn eq(&self, other: &Self) -> bool {
        return other.m_data[0] == self.m_data[0] && other.m_data[1] == self.m_data[1] && other.m_data[2] == self.m_data[2];
    }
}

impl<Real: Float> Eq for ChVector<Real> {}

impl<Real: Float + Ord> Ord for ChVector<Real> {
    fn cmp(&self, other: &Self) -> Ordering {
        return self.m_data.cmp(&other.m_data);
    }
}

impl<Real: Float + Ord> PartialOrd for ChVector<Real> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        return Some(self.cmp(other));
    }
}

//--------------------------------------------------------------------------------------------------
/// Shortcut for faster use of typical double-precision vectors.
/// <pre>
/// Instead of writing
///    ChVector<double> foo;
/// or
///    ChVector<> foo;
/// you can use the shorter version
///    Vector foo;
/// </pre>
pub type Vector = ChVector<f64>;

/// Shortcut for faster use of typical single-precision vectors.
/// <pre>
/// Instead of writing
///    ChVector<float> foo;
/// you can use the shorter version
///    Vector foo;
/// </pre>
pub type VectorF = ChVector<f32>;

//--------------------------------------------------------------------------------------------------
// CONSTANTS

pub const VNULL: ChVector<f64> = ChVector { m_data: [0.0, 0.0, 0.0] };
pub const VECT_X: ChVector<f64> = ChVector { m_data: [1.0, 0.0, 0.0] };
pub const VECT_Y: ChVector<f64> = ChVector { m_data: [0.0, 1.0, 0.0] };
pub const VECT_Z: ChVector<f64> = ChVector { m_data: [0.0, 0.0, 1.0] };

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
#[cfg(test)]
mod ch_vector_test {
    use crate::ch_vector::ChVector;
    use num::traits::real::Real;

    const ABS_ERR_D: f64 = 1e-15;
    const ABS_ERR_F: f32 = 1e-6;

    #[macro_export]
    macro_rules! expect_near {
        ($x:expr, $y:expr, $d:expr) => {
            assert_eq!(($x - $y).abs() < $d, true, "{}, {}", $x - $y, $d);
        }
    }

    #[test]
    fn normalize() {
        let mut ad = ChVector::new(1.1, -2.2, 3.3);
        expect_near!(ad.get_normalized().length(), 1.0, ABS_ERR_D);
        assert_eq!(ad.normalize(), true);
        expect_near!(ad.length(), 1.0, ABS_ERR_D);

        let mut bd = ChVector::new_scalar(0.0);
        assert_eq!(bd.normalize(), false);

        let mut af = ChVector::<f32>::new(1.1, -2.2, 3.3);
        expect_near!(af.get_normalized().length(), 1.0, ABS_ERR_F);
        assert_eq!(af.normalize(), true);
        expect_near!(af.length(), 1.0, ABS_ERR_F);

        let mut bf = ChVector::<f32>::new_scalar(0.0);
        assert_eq!(bf.normalize(), false);
    }

    #[test]
    fn dot() {
        let ad = ChVector::new(1.1, -2.2, 3.3);
        expect_near!(ad.dot(&ad), ad.length2(), ABS_ERR_D);
        expect_near!(ad.dot(&-ad.clone()), -ad.length2(), ABS_ERR_D);
        expect_near!(ad.dot(&ad.get_orthogonal_vector()), 0.0, ABS_ERR_D);

        let af = ChVector::<f32>::new(1.1, -2.2, 3.3);
        expect_near!(af.dot(&af), af.length2(), ABS_ERR_F);
        expect_near!(af.dot(&-af.clone()), -af.length2(), ABS_ERR_F);
        expect_near!(af.dot(&af.get_orthogonal_vector()), 0.0, ABS_ERR_F);
    }

    #[test]
    fn cross() {
        let ad = ChVector::new(1.1, -2.2, 3.3);
        let bd = ChVector::new(-0.5, 0.6, 0.7);
        let cd = ad.cross(&bd);
        expect_near!(cd.dot(&ad), 0.0, ABS_ERR_D);
        expect_near!(cd.dot(&bd), 0.0, ABS_ERR_D);

        let zd1 = ad.cross(&ad);
        expect_near!(zd1.x(), 0.0, ABS_ERR_D);
        expect_near!(zd1.y(), 0.0, ABS_ERR_D);
        expect_near!(zd1.z(), 0.0, ABS_ERR_D);

        let zd2 = ad.cross(&-ad.clone());
        expect_near!(zd2.x(), 0.0, ABS_ERR_D);
        expect_near!(zd2.y(), 0.0, ABS_ERR_D);
        expect_near!(zd2.z(), 0.0, ABS_ERR_D);

        let pd = ad.get_orthogonal_vector();
        expect_near!(ad.cross(&pd).length(), ad.length() * pd.length(), ABS_ERR_D);

        let af = ChVector::<f32>::new(1.1, -2.2, 3.3);
        let bf = ChVector::<f32>::new(-0.5, 0.6, 0.7);
        let cf = af.cross(&bf);
        expect_near!(cf.dot(&af), 0.0, ABS_ERR_F);
        expect_near!(cf.dot(&bf), 0.0, ABS_ERR_F);

        let zf1 = af.cross(&af);
        expect_near!(zf1.x(), 0.0, ABS_ERR_F);
        expect_near!(zf1.y(), 0.0, ABS_ERR_F);
        expect_near!(zf1.z(), 0.0, ABS_ERR_F);

        let zf2 = af.cross(&-af.clone());
        expect_near!(zf2.x(), 0.0, ABS_ERR_F);
        expect_near!(zf2.y(), 0.0, ABS_ERR_F);
        expect_near!(zf2.z(), 0.0, ABS_ERR_F);

        let pf = af.get_orthogonal_vector();
        expect_near!(af.cross(&pf).length(), af.length() * pf.length(), ABS_ERR_F);
    }
}
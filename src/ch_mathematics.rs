/*
 * // Copyright (c) 2021 Feng Yang
 * //
 * // I am making my contributions/submissions to this project solely in my
 * // personal capacity and am not conveying any rights to any intellectual
 * // property of any third parties.
 */

use std::f64::consts::*;
use num::Float;

pub const CH_C_PI: f64 = PI;
pub const CH_C_PI_2: f64 = FRAC_PI_2;
pub const CH_C_PI_4: f64 = FRAC_PI_4;
pub const CH_C_1_PI: f64 = FRAC_1_PI;
pub const CH_C_2PI: f64 = 6.283185307179586476925286766559;
pub const CH_C_RAD_TO_DEG: f64 = 180.0 / PI;
pub const CH_C_DEG_TO_RAD: f64 = PI / 180.0;

pub const CH_C_SQRT_2: f64 = SQRT_2;
pub const CH_C_SQRT_1_2: f64 = FRAC_1_SQRT_2;

pub const CH_C_E: f64 = E;
pub const CH_C_LOG2E: f64 = LOG2_E;
pub const CH_C_LOG10E: f64 = LOG10_E;
pub const CH_C_LN2: f64 = LN_2;
pub const CH_C_LN10: f64 = LN_10;

pub const BDF_STEP_HIGH: f64 = 1e-4;
pub const BDF_STEP_LOW: f64 = 1e-7;

/// Computes the atan2, returning angle given cosine and sine.
pub fn ch_atan2<T: Float>(mcos: T, msin: T) -> T {
    let mut ret;
    if mcos.abs() < T::from(0.707).unwrap() {
        ret = mcos.acos();
        if msin < T::zero() {
            ret = -ret;
        }
    } else {
        ret = msin.asin();
        if mcos < T::zero() {
            ret = T::from(CH_C_PI).unwrap() - ret;
        }
    }
    return ret;
}

/// Clamp and modify the specified value to lie within the given limits.
pub fn ch_clamp_value<T: num::Num + PartialOrd>(value: &mut T, limit_min: T, limit_max: T) {
    if *value < limit_min {
        *value = limit_min;
    } else if *value > limit_max {
        *value = limit_max;
    }
}

/// Clamp the specified value to lie within the given limits.
pub fn ch_clamp<T: Float>(value: T, limit_min: T, limit_max: T) -> T {
    if value < limit_min {
        return limit_min;
    }
    if value > limit_max {
        return limit_max;
    }
    return value;
}

/// Signum function.
pub fn ch_signum<T: Float>(x: T) -> i32 { return (x > T::zero()) as i32 - (x < T::zero()) as i32; }

/// Smooth (sinusoidal) ramp between y1 and y2.
/// Note: must have x1 < x2 (no check).
pub fn ch_sine_step<T: Float>(x: T, x1: T, y1: T, x2: T, y2: T) -> T {
    if x <= x1 {
        return y1;
    }
    if x >= x2 {
        return y2;
    }
    let dx = x2 - x1;
    let dy = y2 - y1;
    let y = y1 + dy * (x - x1) / dx - (dy / T::from(CH_C_2PI).unwrap()) * (T::from(CH_C_2PI).unwrap() * (x - x1) / dx).sin();
    return y;
}

/// Parameter make periodic in 0..1
/// (using 0..1 modulus if closed, otherwise clamping in 0..1 range)
pub fn ch_periodic_par<T: Float>(u: &mut T, closed: bool) {
    if *u < T::zero() {
        if closed {
            *u = *u + T::one();
        } else {
            *u = T::zero();
        }
    }
    if *u > T::one() {
        if closed {
            *u = *u - T::one();
        } else {
            *u = T::one();
        }
    }
}